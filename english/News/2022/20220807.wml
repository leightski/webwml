<define-tag pagetitle>Ownership of <q>debian.community</q> domain</define-tag>
<define-tag release_date>2022-08-07</define-tag>
#use wml::debian::news

<p>
The World Intellectual Property Organization (WIPO), under its
Uniform Domain-Name Dispute-Resolution Policy (UDRP),
decided that ownership of the <q><a href="https://debian.community">debian.community</a></q> domain
should be <a href="https://www.wipo.int/amc/en/domains/search/case.jsp?case=D2022-1524">transferred to the Debian Project</a>.
</p>

<p>
The appointed panel found that <q>the disputed domain name is identical to a trademark
in which the Complainant has rights.</q>
</p>

<p>
In its decision the panel noted:
<p>

<blockquote>
[...] the disputed domain name is identical to the DEBIAN mark, which carries a high risk
of implied affiliation with the Complainant. [...] Given that the Complainant prominently
describes Debian on its homepage as a 'community' and not just an operating system,
[the .community] suffix actually reinforces that the disputed domain name will resolve to a
site operated or endorsed by [Debian]. The disputed domain name contains no critical or
other terms to dispel or qualify that false impression.
</blockquote>

<p>
The panel went on to observe:
</p>

<blockquote>
The evidence submitted by the Complainant shows that some posts present the DEBIAN
mark together with information about a notorious sex cult, notorious sex offenders, and
enslavement of women, and one post displays photographs of physical branding allegedly on
victims’ genital skin. The segues from information about the Complainant to this type of
information are contrived and the scale of this information is not merely incidental on the
website. In the Panel’s view, these posts are deliberately intended to create a false
association between the DEBIAN trademark and offensive phenomena and thereby tarnish
the mark.
</blockquote>

<p>
and further concluded that:
</p>

<blockquote>
nothing in the Debian Social Contract or elsewhere indicates that the Complainant has ever
consented to the type of false associations with its mark published by the respondent on its
website. The Respondent points out that the DEBIAN mark is registered only in respect of
software. However, while the relevant posts attack members of the Complainant who make
available DEBIAN software, rather than the software itself, these posts use the mark in
combination with the disputed domain name in a way that intentionally seeks to create false
associations with the mark itself.
</blockquote>

<p>
Debian is committed to the proper use of its trademarks subject to its 
<a href="$(HOME)/trademark">trademark policy</a>
and will continue to take enforcement action when that policy is violated.
</p>

<p>
The content of <q>debian.community</q> has now been replaced by a 
<a href="$(HOME)/legal/debian-community-site">page</a>
explaining the situation and answering further questions.
</p>

<p>
WIPO's full decision text is available online:
<a href="https://www.wipo.int/amc/en/domains/search/case.jsp?case_id=58273">https://www.wipo.int/amc/en/domains/search/case.jsp?case_id=58273</a>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely free
operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.</p>
