<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Steven Seeley discovered that in jetty, a Java servlet engine and
webserver, requests to the ConcatServlet and WelcomeFilter are able to
access protected resources within the WEB-INF directory. An attacker
may access sensitive information regarding the implementation of a web
application.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
9.2.30-0+deb9u2.</p>

<p>We recommend that you upgrade your jetty9 packages.</p>

<p>For the detailed security status of jetty9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jetty9">https://security-tracker.debian.org/tracker/jetty9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2688.data"
# $Id: $
