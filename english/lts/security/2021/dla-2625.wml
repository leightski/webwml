<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Debian courier-authlib package before 0.71.1-2 for Courier
Authentication Library creates a /run/courier/authdaemon
directory with weak permissions, allowing an attacker to read
user information. This may include a cleartext password in some
configurations. In general, it includes the user's existence,
uid and gids, home and/or Maildir directory, quota, and some
type of password information (such as a hash).</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.66.4-9+deb9u1.</p>

<p>We recommend that you upgrade your courier-authlib packages.</p>

<p>For the detailed security status of courier-authlib please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/courier-authlib">https://security-tracker.debian.org/tracker/courier-authlib</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2625.data"
# $Id: $
