<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an in issue in the command-line tool for
the <a href="https://clusterlabs.org/pacemaker/">Pacemaker High Availability
stack</a>. Local attackers were able to execute commands via shell code
injection to the <tt>crm history</tt> command-line tool, potentially allowing
escalation of privileges.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35459">CVE-2020-35459</a>

    <p>An issue was discovered in ClusterLabs crmsh through 4.2.1. Local
    attackers able to call "crm history" (when "crm" is run) were able to
    execute commands via shell code injection to the crm history commandline,
    potentially allowing escalation of privileges.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
2.3.2-4+deb9u1.</p>

<p>We recommend that you upgrade your crmsh packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2533.data"
# $Id: $
