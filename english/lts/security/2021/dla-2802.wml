<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in elfutils, a collection of
utilities and libraries to handle ELF objects.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16062">CVE-2018-16062</a>

    <p>dwarf_getaranges in dwarf_getaranges.c in libdw allowed a denial of
    service (heap-based buffer over-read) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16402">CVE-2018-16402</a>

    <p>libelf/elf_end.c in allowed to cause a denial of service (double
    free and application crash) because it tried to decompress twice.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18310">CVE-2018-18310</a>

    <p>An invalid memory address dereference libdwfl allowed a denial of
    service (application crash) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18520">CVE-2018-18520</a>

    <p>A use-after-free in recursive ELF ar files allowed a denial of
    service (application crash) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18521">CVE-2018-18521</a>

    <p>A divide-by-zero in arlib_add_symbols() allowed a denial of service
    (application crash) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7150">CVE-2019-7150</a>

    <p>A segmentation fault could occur due to dwfl_segment_report_module()
    not checking whether the dyn data read from a core file is truncated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7665">CVE-2019-7665</a>

    <p>NT_PLATFORM core notes contain a zero terminated string allowed a
    denial of service (application crash) via a crafted file.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.168-1+deb9u1.</p>

<p>We recommend that you upgrade your elfutils packages.</p>

<p>For the detailed security status of elfutils please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/elfutils">https://security-tracker.debian.org/tracker/elfutils</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2802.data"
# $Id: $
