<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in netkit-rsh, client and server programs for
remote shell connections.
Due to insufficient input validation in path names sent by server, a
malicious server can do arbitrary file overwrites in the target directory
or modify permissions of the target directory.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
0.17-17+deb9u1.</p>

<p>We recommend that you upgrade your netkit-rsh packages.</p>

<p>For the detailed security status of netkit-rsh please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/netkit-rsh">https://security-tracker.debian.org/tracker/netkit-rsh</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2822.data"
# $Id: $
