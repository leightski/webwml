<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<ul>

    <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5093">CVE-2016-5093</a>

      <p>Absence of null character causes unexpected zend_string length and
      leaks heap memory. The test script uses locale_get_primary_language
      to reach get_icu_value_internal but there are some other functions
      that also trigger this issue:<br>
        locale_canonicalize, locale_filter_matches,<br>
        locale_lookup, locale_parse</p></li>

    <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5094">CVE-2016-5094</a>

      <p>don't create strings with lengths outside int range</p></li>

    <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5095">CVE-2016-5095</a>

      <p>similar to <a href="https://security-tracker.debian.org/tracker/CVE-2016-5094">CVE-2016-5094</a>
      don't create strings with lengths outside int range</p></li>

    <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5096">CVE-2016-5096</a>

      <p>int/size_t confusion in fread</p></li>

    <li>CVE-TEMP-bug-70661

      <p>bug70661: Use After Free Vulnerability in WDDX Packet Deserialization</p></li>

    <li>CVE-TEMP-bug-70728

      <p>bug70728: Type Confusion Vulnerability in PHP_to_XMLRPC_worker()</p></li>

    <li>CVE-TEMP-bug-70741

      <p>bug70741: Session WDDX Packet Deserialization Type Confusion
                Vulnerability</p></li>

    <li>CVE-TEMP-bug-70480-raw

      <p>bug70480: php_url_parse_ex() buffer overflow read</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.4.45-0+deb7u4.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-533.data"
# $Id: $
