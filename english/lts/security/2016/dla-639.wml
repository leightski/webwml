<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7115">CVE-2016-7115</a>
    <p>Buffer overflow in the handle_packet function in mactelnet.c in the
    client in MAC-Telnet 0.4.3 and earlier allows remote TELNET servers to
    execute arbitrary code via a long string in an MT_CPTYPE_ENCRYPTIONKEY
    control packet.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.3.4-1+deb7u1.</p>

<p>We recommend that you upgrade your mactelnet packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-639.data"
# $Id: $
