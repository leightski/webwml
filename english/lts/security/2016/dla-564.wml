<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were found in tardiff:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-0857">CVE-2015-0857</a>

    <p>Arbitrary command execution was possible via shell metacharacters
    in the name of a (1) tar file or (2) file within a tar file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-0858">CVE-2015-0858</a>

    <p>Local users could write to arbitrary files via a symlink attack on
    a pathname in a /tmp/tardiff-$$ temporary directory.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.1-1+deb7u1.</p>

<p>We recommend that you upgrade your tardiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-564.data"
# $Id: $
