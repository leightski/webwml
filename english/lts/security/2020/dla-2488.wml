<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Various memory and file descriptor leaks were discovered in the Python
interface to the APT package management runtime library, which could
result in denial of service.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.4.2.</p>

<p>We recommend that you upgrade your python-apt packages.</p>

<p>For the detailed security status of python-apt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-apt">https://security-tracker.debian.org/tracker/python-apt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2488.data"
# $Id: $
