<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The following CVE(s) were reported against src:intel-microcode.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>

    <p>A new domain bypass transient execution attack known as Special
    Register Buffer Data Sampling (SRBDS) has been found. This flaw
    allows data values from special internal registers to be leaked
    by an attacker able to execute code on any core of the CPU. An
    unprivileged, local attacker can use this flaw to infer values
    returned by affected instructions known to be commonly used
    during cryptographic operations that rely on uniqueness, secrecy,
    or both.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0548">CVE-2020-0548</a>

    <p>A flaw was found in Intel processors where a local attacker is
    able to gain information about registers used for vector
    calculations by observing register states from other processes
    running on the system. This results in a race condition where
    store buffers, which were not cleared, could be read by another
    process or a CPU sibling. The highest threat from this
    vulnerability is data confidentiality where an attacker could
    read arbitrary data as it passes through the processor.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0549">CVE-2020-0549</a>

    <p>A microarchitectural timing flaw was found on some Intel
    processors. A corner case exists where data in-flight during the
    eviction process can end up in the “fill buffers” and not properly
    cleared by the MDS mitigations. The fill buffer contents (which
    were expected to be blank) can be inferred using MDS or TAA style
    attack methods to allow a local attacker to infer fill buffer
    values.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.20200609.2~deb8u1.</p>

<p>We recommend that you upgrade your intel-microcode packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2248.data"
# $Id: $
