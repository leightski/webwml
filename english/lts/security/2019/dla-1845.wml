<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in DOSBox, an
emulator for running old DOS programs.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7165">CVE-2019-7165</a>

    <p>A very long line inside a bat file would overflow the parsing buffer
    which could be used by an attacker to execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12594">CVE-2019-12594</a>

    <p>Insufficient access controls inside DOSBox allowed attackers to
    access resources on the host system and execute arbitrary code.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.74-4+deb8u1.</p>

<p>We recommend that you upgrade your dosbox packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1845.data"
# $Id: $
