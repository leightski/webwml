<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18249">CVE-2017-18249</a>

    <p>A race condition was discovered in the disk space allocator of
    F2FS. A user with access to an F2FS volume could use this to cause
    a denial of service or other security impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1128">CVE-2018-1128</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-1129">CVE-2018-1129</a>

    <p>The cephx authentication protocol used by Ceph was susceptible to
    replay attacks, and calculated signatures incorrectly. These
    vulnerabilities in the server required changes to authentication
    that are incompatible with existing clients. The kernel's client
    code has now been updated to be compatible with the fixed server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">CVE-2018-3639</a> (SSB)

    <p>Multiple researchers have discovered that Speculative Store Bypass
    (SSB), a feature implemented in many processors, could be used to
    read sensitive information from another context. In particular,
    code in a software sandbox may be able to read sensitive
    information from outside the sandbox. This issue is also known as
    Spectre variant 4.</p>

    <p>This update adds a further mitigation for this issue in the eBPF
    (Extended Berkeley Packet Filter) implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5391">CVE-2018-5391</a> (FragmentSmack)

    <p>Juha-Matti Tilli discovered a flaw in the way the Linux kernel
    handled reassembly of fragmented IPv4 and IPv6 packets. A remote
    attacker can take advantage of this flaw to trigger time and
    calculation expensive fragment reassembly algorithms by sending
    specially crafted packets, leading to remote denial of service.</p>

    <p>This was previously mitigated by reducing the default limits on
    memory usage for incomplete fragmented packets. This update
    replaces that mitigation with a more complete fix.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5848">CVE-2018-5848</a>

    <p>The wil6210 wifi driver did not properly validate lengths in scan
    and connection requests, leading to a possible buffer overflow.
    On systems using this driver, a local user with the CAP_NET_ADMIN
    capability could use this for denial of service (memory corruption
    or crash) or potentially for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12896">CVE-2018-12896</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-13053">CVE-2018-13053</a>

    <p>Team OWL337 reported possible integer overflows in the POSIX
    timer implementation. These might have some security impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13096">CVE-2018-13096</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-13097">CVE-2018-13097</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-13100">CVE-2018-13100</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-14614">CVE-2018-14614</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-14616">CVE-2018-14616</a>

    <p>Wen Xu from SSLab at Gatech reported that crafted F2FS volumes
    could trigger a crash (BUG, Oops, or division by zero) and/or
    out-of-bounds memory access. An attacker able to mount such a
    volume could use this to cause a denial of service or possibly for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13406">CVE-2018-13406</a>

    <p>Dr Silvio Cesare of InfoSect reported a potential integer overflow
    in the uvesafb driver. A user with permission to access such a
    device might be able to use this for denial of service or
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14610">CVE-2018-14610</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-14611">CVE-2018-14611</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-14612">CVE-2018-14612</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-14613">CVE-2018-14613</a>

    <p>Wen Xu from SSLab at Gatech reported that crafted Btrfs volumes
    could trigger a crash (Oops) and/or out-of-bounds memory access.
    An attacker able to mount such a volume could use this to cause a
    denial of service or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15471">CVE-2018-15471</a> (XSA-270)

    <p>Felix Wilhelm of Google Project Zero discovered a flaw in the hash
    handling of the xen-netback Linux kernel module. A malicious or
    buggy frontend may cause the (usually privileged) backend to make
    out of bounds memory accesses, potentially resulting in privilege
    escalation, denial of service, or information leaks.</p>

    <p>https://xenbits.xen.org/xsa/advisory-270.html</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16862">CVE-2018-16862</a>

    <p>Vasily Averin and Pavel Tikhomirov from Virtuozzo Kernel Team
    discovered that the cleancache memory management feature did not
    invalidate cached data for deleted files. On Xen guests using the
    tmem driver, local users could potentially read data from other
    users' deleted files if they were able to create new files on the
    same volume.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17972">CVE-2018-17972</a>

    <p>Jann Horn reported that the /proc/*/stack files in procfs leaked
    sensitive data from the kernel. These files are now only readable
    by users with the CAP_SYS_ADMIN capability (usually only root)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18281">CVE-2018-18281</a>

    <p>Jann Horn reported a race condition in the virtual memory manager
    that can result in a process briefly having access to memory after
    it is freed and reallocated. A local user could possibly exploit
    this for denial of service (memory corruption) or for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18690">CVE-2018-18690</a>

    <p>Kanda Motohiro reported that XFS did not correctly handle some
    xattr (extended attribute) writes that require changing the disk
    format of the xattr. A user with access to an XFS volume could use
    this for denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18710">CVE-2018-18710</a>

    <p>It was discovered that the cdrom driver does not correctly
    validate the parameter to the CDROM_SELECT_DISC ioctl. A user with
    access to a cdrom device could use this to read sensitive
    information from the kernel or to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19407">CVE-2018-19407</a>

    <p>Wei Wu reported a potential crash (Oops) in the KVM implementation
    for x86 processors. A user with access to /dev/kvm could use this
    for denial of service.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.9.144-3.1~deb8u1.  This version also includes fixes for Debian bugs
#890034, #896911, #907581, #915229, and #915231; and other fixes
included in upstream stable updates.</p>

<p>We recommend that you upgrade your linux-4.9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1715.data"
# $Id: $
