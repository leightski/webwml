<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that various procedures in Ghostscript, the GPL
PostScript/PDF interpreter, do not properly restrict privileged calls,
which could result in bypass of file system restrictions of the dSAFER
sandbox.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
9.26a~dfsg-0+deb8u5.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1915.data"
# $Id: $
