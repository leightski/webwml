<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in imagemagick, an image processing
toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11470">CVE-2019-11470</a>

    <p>Uncontrolled resource consumption caused by insufficiently sanitized image
    size in ReadCINImage (coders/cin.c). This vulnerability might be leveraged
    by remote attackers to cause denial of service via a crafted Cineon image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14981">CVE-2019-14981</a>

    <p>Divide-by-zero vulnerability in MeanShiftImage (magick/feature.c). This
    vulnerability might be leveraged by remote attackers to cause denial of
    service via crafted image data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15139">CVE-2019-15139</a>

    <p>Out-of-bounds read in ReadXWDImage (coders/xwd.c). This vulnerability might
    be leveraged by remote attackers to cause denial of service via a crafted
    XWD (X Window System window dumping file) image file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15140">CVE-2019-15140</a>

    <p>Bound checking issue in ReadMATImage (coders/mat.c), potentially leading to
    use-after-free. This vulnerability might be leveraged by remote attackers to
    cause denial of service or any other unspecified impact via a crafted MAT
    image file.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
8:6.8.9.9-5+deb8u18.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1968.data"
# $Id: $
