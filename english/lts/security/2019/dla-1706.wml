<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in the poppler PDF
rendering shared library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19058">CVE-2018-19058</a>

    <p>A reachable abort in Object.h will lead to denial-of-service because
    EmbFile::save2 in FileSpec.cc lacks a stream check before saving an
    embedded file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20481">CVE-2018-20481</a>

    <p>Poppler mishandles unallocated XRef entries, which allows remote
    attackers to cause a denial-of-service (NULL pointer dereference)
    via a crafted PDF document.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20662">CVE-2018-20662</a>

    <p>Poppler allows attackers to cause a denial-of-service (application
    crash and segmentation fault by crafting a PDF file in which an xref
    data structure is corrupted.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7310">CVE-2019-7310</a>

    <p>A heap-based buffer over-read (due to an integer signedness error in
    the XRef::getEntry function in XRef.cc) allows remote attackers to
    cause a denial of service (application crash) or possibly have
    unspecified other impact via a crafted PDF document.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9200">CVE-2019-9200</a>

    <p>A heap-based buffer underwrite exists in ImageStream::getLine()
    located at Stream.cc that can (for example) be triggered by sending
    a crafted PDF file to the pdfimages binary. It allows an attacker to
    cause denial-of-service (segmentation fault) or possibly have
    unspecified other impact.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.26.5-2+deb8u8.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1706.data"
# $Id: $
