<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in mbedtls, a
lightweight crypto and SSL/TLS library, which may allow attackers to obtain
sensitive information like the RSA private key or cause a denial of service
(application or server crash).</p>

<p>For Debian 10 buster, these problems have been fixed in version
2.16.9-0~deb10u1.</p>

<p>We recommend that you upgrade your mbedtls packages.</p>

<p>For the detailed security status of mbedtls please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/mbedtls">https://security-tracker.debian.org/tracker/mbedtls</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3249.data"
# $Id: $
