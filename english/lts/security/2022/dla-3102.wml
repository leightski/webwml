<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Linux 5.10 has been packaged for Debian 10 as linux-5.10.  This
provides a supported upgrade path for systems that currently use
kernel packages from the "buster-backports" suite.</p>

<p>There is no need to upgrade systems using Linux 4.19, as that kernel
version will also continue to be supported in the LTS period.</p>

<p>The "apt full-upgrade" command will <em>not</em> automatically install the
updated kernel packages.  You should explicitly install one of the
following metapackages first, as appropriate for your system:</p>

<ul>
<li>linux-image-5.10-686</li>
<li>linux-image-5.10-686-pae</li>
<li>linux-image-5.10-amd64</li>
<li>linux-image-5.10-arm64</li>
<li>linux-image-5.10-armmp</li>
<li>linux-image-5.10-armmp-lpae</li>
<li>linux-image-5.10-cloud-amd64</li>
<li>linux-image-5.10-cloud-arm64</li>
<li>linux-image-5.10-rt-686-pae</li>
<li>linux-image-5.10-rt-amd64</li>
<li>linux-image-5.10-rt-arm64</li>
<li>linux-image-5.10-rt-armmp</li>
</ul>

<p>For example, if the command "uname -r" currently shows
"5.10.0-0.deb10.16-amd64", you should install linux-image-5.10-amd64.</p>

<p>This backport does not include the following binary packages:</p>

<blockquote><div>bpftool hyperv-daemons libcpupower-dev libcpupower1
linux-compiler-gcc-8-arm linux-compiler-gcc-8-x86 linux-cpupower
linux-libc-dev usbip</div></blockquote>

<p>Older versions of most of those are built from the linux source
package in Debian 10.</p>

<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2585">CVE-2022-2585</a>

    <p>A use-after-free flaw in the implementation of POSIX CPU timers
    may result in denial of service or in local privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2586">CVE-2022-2586</a>

    <p>A use-after-free in the Netfilter subsystem may result in local
    privilege escalation for a user with the CAP_NET_ADMIN capability
    in any user or network namespace.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2588">CVE-2022-2588</a>

    <p>Zhenpeng Lin discovered a use-after-free flaw in the cls_route
    filter implementation which may result in local privilege
    escalation for a user with the CAP_NET_ADMIN capability in any
    user or network namespace.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26373">CVE-2022-26373</a>

    <p>It was discovered that on certain processors with Intel's Enhanced
    Indirect Branch Restricted Speculation (eIBRS) capabilities there
    are exceptions to the documented properties in some situations,
    which may result in information disclosure.</p>

    <p>Intel's explanation of the issue can be found at
    <a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/post-barrier-return-stack-buffer-predictions.html">https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/post-barrier-return-stack-buffer-predictions.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29900">CVE-2022-29900</a>

    <p>Johannes Wikner and Kaveh Razavi reported that for AMD/Hygon
    processors, mis-trained branch predictions for return instructions
    may allow arbitrary speculative code execution under certain
    microarchitecture-dependent conditions.</p>

    <p>A list of affected AMD CPU types can be found at
    <a href="https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-1037">https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-1037</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29901">CVE-2022-29901</a>

    <p>Johannes Wikner and Kaveh Razavi reported that for Intel
    processors (Intel Core generation 6, 7 and 8), protections against
    speculative branch target injection attacks were insufficient in
    some circumstances, which may allow arbitrary speculative code
    execution under certain microarchitecture-dependent conditions.</p>

    <p>More information can be found at
    <a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/return-stack-buffer-underflow.html">https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/return-stack-buffer-underflow.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36879">CVE-2022-36879</a>

    <p>A flaw was discovered in xfrm_expand_policies in the xfrm
    subsystem which can cause a reference count to be dropped twice.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36946">CVE-2022-36946</a>

    <p>Domingo Dirutigliano and Nicola Guerrera reported a memory
    corruption flaw in the Netfilter subsystem which may result in
    denial of service.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
5.10.136-1~deb10u3. This update additionally includes many more bug
fixes from stable updates 5.10.128-5.10.136 inclusive.</p>

<p>We recommend that you upgrade your linux-5.10 packages.</p>

<p>For the detailed security status of linux-5.10 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux-5.10">https://security-tracker.debian.org/tracker/linux-5.10</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3102.data"
# $Id: $
