<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Timothee Desurmont discovered an information leak vulnerability in
node-eventsource, a W3C compliant EventSource client for Node.js: the
module was not honoring the same-origin-policy and upon following a
redirect would leak cookies to the the target URL.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.2.1-1+deb10u1.</p>

<p>We recommend that you upgrade your node-eventsource packages.</p>

<p>For the detailed security status of node-eventsource please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-eventsource">https://security-tracker.debian.org/tracker/node-eventsource</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3235.data"
# $Id: $
