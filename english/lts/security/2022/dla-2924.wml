<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential remote denial of service (DoS)
attack in XStream, a Java library used to serialize objects to XML and back
again.</p>

<p>An attacker could have consumed 100% of the CPU resources, but the library
now monitors and accumulates the time it takes to add elements to collections,
and throws an exception if a set threshold is exceeded.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43859">CVE-2021-43859</a>

    <p>XStream is an open source java library to serialize objects to XML and
    back again. Versions prior to 1.4.19 may allow a remote attacker to
    allocate 100% CPU time on the target system depending on CPU type or
    parallel execution of such a payload resulting in a denial of service only
    by manipulating the processed input stream. XStream 1.4.19 monitors and
    accumulates the time it takes to add elements to collections and throws an
    exception if a set threshold is exceeded. Users are advised to upgrade as
    soon as possible. Users unable to upgrade may set the NO_REFERENCE mode to
    prevent recursion. See GHSA-rmr5-cpv2-vgjf for further details on a
    workaround if an upgrade is not possible.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1.4.11.1-1+deb9u5.</p>

<p>We recommend that you upgrade your libxstream-java packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2924.data"
# $Id: $
