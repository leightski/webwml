<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in Emacs where where attackers
could have executed arbitrary commands via shell metacharacters in the name of
a source-code file.</p>

<p>This was because lib-src/etags.c used the system(3) library function when
calling the (external) ctags(1) binary.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45939">CVE-2022-45939</a>

    <p>GNU Emacs through 28.2 allows attackers to execute commands via shell
    metacharacters in the name of a source-code file, because lib-src/etags.c
    uses the system C library function in its implementation of the ctags
    program. For example, a victim may use the "ctags *" command (suggested in
    the ctags documentation) in a situation where the current working directory
    has contents that depend on untrusted input.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1:26.1+1-3.2+deb10u3.</p>

<p>We recommend that you upgrade your emacs packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3257.data"
# $Id: $
