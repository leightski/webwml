<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were discovered in Epiphany, the GNOME web browser, allowing
XSS attacks by malicious websites, or memory corruption and application
crash by a page with a very long title.</p>

<p>For Debian 10 buster, these problems have been fixed in version
3.32.1.2-3~deb10u2.</p>

<p>We recommend that you upgrade your epiphany-browser packages.</p>

<p>For the detailed security status of epiphany-browser please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/epiphany-browser">https://security-tracker.debian.org/tracker/epiphany-browser</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3074.data"
# $Id: $
