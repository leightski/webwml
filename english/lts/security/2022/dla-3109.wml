<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that authenticated users could trigger a fault in Nova, a
cloud computing fabric controller, to cause information leak.</p>

<p>In addition, this update includes some fixes for volume live migration,
as well as the addition of a /healthcheck URL for monitoring.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2:18.1.0-6+deb10u1.</p>

<p>We recommend that you upgrade your nova packages.</p>

<p>For the detailed security status of nova please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/nova">https://security-tracker.debian.org/tracker/nova</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3109.data"
# $Id: $
