<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Jan-Niklas Sohn discovered several vulnerabilities in X server extensions
in the X.Org X server, which may result in privilege escalation if the X
server is running privileged.</p>


<p>For Debian 10 buster, these problems have been fixed in version
2:1.20.4-1+deb10u7.</p>

<p>We recommend that you upgrade your xorg-server packages.</p>

<p>For the detailed security status of xorg-server please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xorg-server">https://security-tracker.debian.org/tracker/xorg-server</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3256.data"
# $Id: $
