<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in lrzsz, a set of tools for zmodem/xmodem/ymodem
file transfer.
Due to an incorrect length check, which might result in a size_t wrap
around, an information leak to the receiving side could happen.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
0.12.21-8+deb9u1.</p>

<p>We recommend that you upgrade your lrzsz packages.</p>

<p>For the detailed security status of lrzsz please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lrzsz">https://security-tracker.debian.org/tracker/lrzsz</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2900.data"
# $Id: $
