<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in the Twisted Python network
framework where SSH client and server implementions could accept an infinite
amount of data for the peer's SSH version identifier and that a buffer then
uses all available memory.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21716">CVE-2022-21716</a>

    <p>CVE-2022-21716: Twisted is an event-based framework for internet
    applications, supporting Python 3.6+. Prior to 22.2.0, Twisted SSH client
    and server implement is able to accept an infinite amount of data for the
    peer's SSH version identifier. This ends up with a buffer using all the
    available memory. The attach is a simple as `nc -rv localhost 22 &lt;
    /dev/zero`. A patch is available in version 22.2.0. There are currently no
    known workarounds.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, this problem has been fixed in version
16.6.0-2+deb9u2.</p>

<p>We recommend that you upgrade your twisted packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2938.data"
# $Id: $
