<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Smarty3, a template engine for PHP, allowed template authors to run restricted
static php methods. The same authors could also run arbitrary PHP code by
crafting a malicious math string. If a math string was passed through as user
provided data to the math function, remote users were able to run arbitrary PHP
code as well.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
3.1.31+20161214.1.c7d42e4+selfpack1-2+deb9u5.</p>

<p>We recommend that you upgrade your smarty3 packages.</p>

<p>For the detailed security status of smarty3 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/smarty3">https://security-tracker.debian.org/tracker/smarty3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2995.data"
# $Id: $
