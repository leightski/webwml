<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An issue was discovered in OpenStack Cinder, a Block Storage service
for OpenStack. By supplying a specially created VMDK flat image that
references a specific backing file path, an authenticated user may
convince systems to return a copy of that file's contents from the
server, resulting in unauthorized access to potentially sensitive data.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2:13.0.7-1+deb10u2.</p>

<p>We recommend that you upgrade your cinder packages.</p>

<p>For the detailed security status of cinder please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cinder">https://security-tracker.debian.org/tracker/cinder</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3301.data"
# $Id: $
