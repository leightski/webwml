<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been discovered in Apache module auth_mellon, which
provides SAML 2.0 authentication.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3877">CVE-2019-3877</a>

    <p>It was possible to bypass the redirect URL checking on logout, so
    the module could be used as an open redirect facility.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3878">CVE-2019-3878</a>

    <p>When mod_auth_mellon is used in an Apache configuration which
    serves as a remote proxy with the http_proxy module, it was
    possible to bypass authentication by sending SAML ECP headers.</p></li>

</ul>

<p>For the stable distribution (stretch), these problems have been fixed in
version 0.12.0-2+deb9u1.</p>

<p>We recommend that you upgrade your libapache2-mod-auth-mellon packages.</p>

<p>For the detailed security status of libapache2-mod-auth-mellon please
refer to its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libapache2-mod-auth-mellon">\
https://security-tracker.debian.org/tracker/libapache2-mod-auth-mellon</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4414.data"
# $Id: $
