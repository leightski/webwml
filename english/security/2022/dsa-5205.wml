<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in Samba, a SMB/CIFS file,
print, and login server for Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2031">CVE-2022-2031</a>

    <p>Luke Howard reported that Samba AD users can bypass certain
    restrictions associated with changing passwords. A user who has been
    requested to change their password can exploit this to obtain and
    use tickets to other services.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32742">CVE-2022-32742</a>

    <p>Luca Moro reported that a SMB1 client with write access to a share
    can cause server memory content to be leaked.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32744">CVE-2022-32744</a>

    <p>Joseph Sutton reported that Samba AD users can forge password change
    requests for any user, resulting in privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32745">CVE-2022-32745</a>

    <p>Joseph Sutton reported that Samba AD users can crash the server
    process with a specially crafted LDAP add or modify request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32746">CVE-2022-32746</a>

    <p>Joseph Sutton and Andrew Bartlett reported that Samba AD users can
    cause a use-after-free in the server process with a specially
    crafted LDAP add or modify request.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2:4.13.13+dfsg-1~deb11u5. The fix for
<a href="https://security-tracker.debian.org/tracker/CVE-2022-32745">CVE-2022-32745</a>
required an update to ldb 2:2.2.3-2~deb11u2 to correct the defect.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>For the detailed security status of samba please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5205.data"
# $Id: $
