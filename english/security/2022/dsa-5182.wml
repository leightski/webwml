<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the WebKitGTK
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22677">CVE-2022-22677</a>

    <p>An anonymous researcher discovered that the video in a webRTC call
    may be interrupted if the audio capture gets interrupted.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26710">CVE-2022-26710</a>

    <p>Chijin Zhou discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 2.36.4-1~deb10u1.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.36.4-1~deb11u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5182.data"
# $Id: $
