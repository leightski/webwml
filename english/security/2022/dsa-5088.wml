<define-tag description>security update</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36740">CVE-2021-36740</a>

    <p>Martin Blix Grydeland discovered that Varnish is vulnerable to
    request smuggling attacks if the HTTP/2 protocol is enabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23959">CVE-2022-23959</a>

    <p>James Kettle discovered a request smuggling attack against the
    HTTP/1 protocol implementation in Varnish.</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 6.1.1-1+deb10u3.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 6.5.1-1+deb11u2.</p>

<p>We recommend that you upgrade your varnish packages.</p>

<p>For the detailed security status of varnish please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/varnish">\
https://security-tracker.debian.org/tracker/varnish</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5088.data"
# $Id: $
