<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the WebKitGTK
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42799">CVE-2022-42799</a>

    <p>Jihwan Kim and Dohyun Lee discovered that visiting a malicious
    website may lead to user interface spoofing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42823">CVE-2022-42823</a>

    <p>Dohyun Lee discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42824">CVE-2022-42824</a>

    <p>Abdulrahman Alqabandi, Ryan Shin and Dohyun Lee discovered that
    processing maliciously crafted web content may disclose sensitive
    user information.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.38.2-1~deb11u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5273.data"
# $Id: $
