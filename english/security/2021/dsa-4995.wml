<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the webkit2gtk
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30846">CVE-2021-30846</a>

    <p>Sergei Glazunov discovered that processing maliciously crafted web
    content may lead to arbitrary code execution</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30851">CVE-2021-30851</a>

    <p>Samuel Gross discovered that processing maliciously crafted web
    content may lead to code execution</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42762">CVE-2021-42762</a>

    <p>An anonymous reporter discovered a limited Bubblewrap sandbox
    bypass that allows a sandboxed process to trick host processes
    into thinking the sandboxed process is not confined.</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 2.34.1-1~deb10u1.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.34.1-1~deb11u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4995.data"
# $Id: $
