#use wml::debian::template title="Сигурност" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="c99908e0effa46ed240da78ee4a1b5ea8e13fcd4"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Поддържане на системата сигурна</a></li>
<li><a href="#DSAS">Последни бюлетини</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Дебиан обръща специално
внимание на сигурността. Обработваме всички съобщения за проблеми със
сигурността и осигуряваме корекции в разумни срокове.</p>
</aside>

<p>
Опитът показва, че <q>сигурност чрез прикриване</q> няма. Публичното разкриване
позволява по-бързи и по-добри решения на проблемите със сигурността. В този
дух, тази страница показва състоянието на Дебиан по отношение на различни
известни проблеми със сигурността, които потенциално засягат Дебиан.
</p>

<p>
Много от бюлетините се координират с други доставчици на свободен софтуер и се
публикуват в деня, когато проблемът се огласи публично.
</p>

# "reasonable timeframe" might be too vague, but we don't have 
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.

<p>
Дебиан участва в стандартизирането на усилията за подобряване на
сигурността:
</p>

<ul>
  <li><a href="#DSAS">бюлетините за сигурността на Дебиан</a> са <a
href="cve-compatibility">съвместими със CVE</a> (<a
href="crossreferences">кръстосан индекс</a>).</li>
  <li>Дебиан има представител в Съвета на
проекта <a href="https://oval.cisecurity.org/">Отворен език за оценка на
пробивите</a>.</li>
</ul>

<h2><a id="keeping-secure">Поддържане на системата сигурна</a></h2>

<p>
За да получавате бюлетините за сигурността на Дебиан се запишете в
пощенския списък <a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a>.
</p>

<p>
Можете да използвате <a href="https://packages.debian.org/stable/admin/apt">apt</a>
за лесно получаване на обновленията по сигурността.
За целта добавете следния ред във файла <code>/etc/apt/sources.list</code>:
</p>

<pre>
deb http://security.debian.org/debian-security <current_release_security_name> main contrib non-free
</pre>

<p>
След запазване на промените, изпълнете следните команди за изтегляне и инсталиране
на наличните обновления:
</p>

<pre>
apt-get update &amp;&amp; apt-get upgrade
</pre>

<p>
Архивът с обновленията по сигурността е подписан с <a
href="https://ftp-master.debian.org/keys.html">ключовете</a>, използвани от
главния архив.
</p>

<p>
За повече информация относно проблемите със сигурността в Дебиан прочетете
отговорите на често задавани въпроси и останалата документация:
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">Често задавани въпроси за сигурността</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Поддържане на сигурността в Дебиан</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Последни бюлетини</a></h2>

<p>Следва сбит списък на бюлетините по сигурността, изпратени до списъка
<a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a>.

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Бюлетини за сигурността в Дебиан (само заглавия)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Бюлетини за сигурността в Дебиан (резюмета)" href="dsa-long">
:#rss#}

<p>
Последните бюлетини за сигурността в Дебиан са достъпни и във <a
href="dsa">формат RDF</a>. Предлагаме и <a href="dsa-long">втори файл</a>,
съдържащ първия абзац от съответния бюлетин, съдържащ информация за засегнатия
пакет.
</p>

#include "$(ENGLISHDIR)/security/index.include"
<p>По-старите бюлетини също са достъпни:
<:= get_past_sec_list(); :>

<p>Дистрибуциите на Дебиан не са уязвими за всички проблеми със сигурността.
<a href="https://security-tracker.debian.org/">Системата за следене на
сигурността</a> съдържа информация за уязвимостта на отделните пакети. В нея
може да се търси по име в CVE или пакет.</p>
