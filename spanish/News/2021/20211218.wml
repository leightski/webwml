#use wml::debian::translation-check translation="43ee4d624248d2eac95a5997ba86398be35914ec"
<define-tag pagetitle>Debian 11 actualizado: publicada la versión 11.2</define-tag>
<define-tag release_date>2021-12-18</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la segunda actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction authheaders "Nueva versión del proyecto original, para corrección de fallos">
<correction base-files "Actualiza /etc/debian_version para la versión 11.2">
<correction bpftrace "Corrige los índices de los arrays">
<correction brltty "Corrige funcionamiento bajo X cuando se usa sysvinit">
<correction btrbk "Corrige regresión en la actualización para corregir CVE-2021-38173">
<correction calibre "Corrige error de sintaxis">
<correction chrony "Corrige la asociación de un socket a un dispositivo de red cuyo nombre tenga más de tres caracteres cuando está habilitado el filtro de llamadas al sistema">
<correction cmake "Añade PostgreSQL 13 a la relación de versiones conocidas">
<correction containerd "Nueva versión «estable» del proyecto original; gestiona el análisis sintáctico de manifiestos OCI ambiguos [CVE-2021-41190]; soporta <q>clone3</q> en el perfil de seccomp por omisión">
<correction curl "Elimina -ffile-prefix-map de curl-config, corrigiendo fallos al intentar instalar simultáneamente libcurl4-gnutls-dev en multiarquitectura">
<correction datatables.js "Corrige insuficiente codificación de arrays para evitar su evaluación («escaping») cuando estos arrays se pasan a la función de codificación de entidades HTML («HTML escape entities function») [CVE-2021-23445]">
<correction debian-edu-config "pxe-addfirmware: corrige ruta del servidor TFTP; mejora soporte de la configuración y mantenimiento de chroot para LTSP">
<correction debian-edu-doc "Actualiza el manual de Debian Edu Bullseye a partir de la wiki; actualiza traducciones">
<correction debian-installer "Recompilado contra proposed-updates; actualiza la ABI del núcleo a la -10">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction distro-info-data "Actualiza los datos incluidos de Ubuntu 14.04 y 16.04 ESM; añade Ubuntu 22.04 LTS">
<correction docker.io "Corrige posible cambio en los permisos del sistema de ficheros del anfitrión [CVE-2021-41089]; asegura permisos de ficheros en /var/lib/docker [CVE-2021-41091]; evita el envío de credenciales al registro por omisión [CVE-2021-41092]; añade soporte de la llamada al sistema <q>clone3</q> en la política de seccomp por omisión">
<correction edk2 "Aborda vulnerabilidad TOCTOU en Boot Guard [CVE-2019-11098]">
<correction freeipmi "Instala los ficheros de pkgconfig en la ubicación correcta">
<correction gdal "Corrige suporte de BAG 2.0 Extract en controlador LVBAG">
<correction gerbv "Corrige problema de escritura fuera de límites [CVE-2021-40391]">
<correction gmp "Corrige problema de desbordamiento de entero y de memoria [CVE-2021-43618]">
<correction golang-1.15 "Nueva versión «estable» del proyecto original; corrige <q>net/http: panic due to racy read of persistConn after handler panic</q> («net/http: panic debido a condición de carrera en la lectura de persistConn tras gestor de panic») [CVE-2021-36221]; corrige <q>archive/zip: overflow in preallocation check can cause OOM panic</q> («archive/zip: desbordamiento en comprobación previa a asignación puede provocar panic OOM») [CVE-2021-39293]; corrige problema de desbordamiento de memoria [CVE-2021-38297], problema de lectura fuera de límites [CVE-2021-41771] y problemas de denegación de servicio [CVE-2021-44716 CVE-2021-44717]">
<correction grass "Corrige análisis sintáctico de formatos GDAL cuando la descripción contiene un signo de dos puntos">
<correction horizon "Habilita de nuevo las traducciones">
<correction htmldoc "Corrige problemas de desbordamiento de memoria [CVE-2021-40985 CVE-2021-43579]">
<correction im-config "Prefiere Fcitx5 a Fcitx4">
<correction isync "Corrige varios problemas de desbordamiento de memoria [CVE-2021-3657]">
<correction jqueryui "Corrige problemas de ejecución de código no confiable [CVE-2021-41182 CVE-2021-41183 CVE-2021-41184]">
<correction jwm "Corrige caída al usar el elemento de menú <q>Move</q>">
<correction keepalived "Corrige política de DBus excesivamente amplia [CVE-2021-44225]">
<correction keystone "Soluciona filtrado de información que permite determinar la existencia de usuarios [CVE-2021-38155]; aplica algunas mejoras de rendimiento al keystone-uwsgi.ini">
<correction kodi "Corrige desbordamiento de memoria en listas de reproducción PLS [CVE-2021-42917]">
<correction libayatana-indicator "Cambia la escala de los iconos al cargar desde fichero; evita caídas habituales en applets de indicadores">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libencode-perl "Corrige una fuga de memoria en Encode.xs">
<correction libseccomp "Añade soporte para llamadas al sistema hasta Linux 5.15">
<correction linux "Nueva versión del proyecto original; incrementa la ABI a la 10; RT: actualiza a 5.10.83-rt58">
<correction linux-signed-amd64 "Nueva versión del proyecto original; incrementa la ABI a la 10; RT: actualiza a 5.10.83-rt58">
<correction linux-signed-arm64 "Nueva versión del proyecto original; incrementa la ABI a la 10; RT: actualiza a 5.10.83-rt58">
<correction linux-signed-i386 "Nueva versión del proyecto original; incrementa la ABI a la 10; RT: actualiza a 5.10.83-rt58">
<correction lldpd "Corrige problema de desbordamiento de memoria dinámica («heap») [CVE-2021-43612]; no establece la etiqueta VLAN si no lo ha hecho el cliente">
<correction mrtg "Corrige errores en nombres de variables">
<correction node-getobject "Resuelve problema de contaminación de prototipo [CVE-2020-28282]">
<correction node-json-schema "Resuelve problema de contaminación de prototipo [CVE-2021-3918]">
<correction open3d "Asegura que python3-open3d depende de python3-numpy">
<correction opendmarc "Corrige opendmarc-import; incrementa la máxima longitud de tokens soportada en cabeceras ARC_Seal, resolviendo caídas">
<correction plib "Corrige problema de desbordamiento de entero [CVE-2021-38714]">
<correction plocate "Corrige un problema por el que se realiza con errores la codificación de caracteres no ASCII para evitar su evaluación («escape»)">
<correction poco "Corrige instalación de ficheros CMake">
<correction privoxy "Corrige fugas de memoria [CVE-2021-44540 CVE-2021-44541 CVE-2021-44542]; corrige problema de ejecución de scripts entre sitios («cross-site scripting») [CVE-2021-44543]">
<correction publicsuffix "Actualiza los datos incluidos">
<correction python-django "Nueva actualización de seguridad del proyecto original: corrige potencial elusión de un control de acceso basado en rutas URL [CVE-2021-44420]">
<correction python-eventlet "Corrige compatibilidad con dnspython 2">
<correction python-virtualenv "Corrige caída cuando se utiliza --no-setuptools">
<correction ros-ros-comm "Corrige problema de denegación de servicio [CVE-2021-37146]">
<correction ruby-httpclient "Usa almacén de certificados del sistema">
<correction rustc-mozilla "Nuevo paquete fuente para soportar la compilación de versiones más recientes de firefox-esr y de thunderbird">
<correction supysonic "Carga jquery a través de un enlace simbólico en lugar de cargarlo directamente; define los enlaces simbólicos a los ficheros CSS de arranque correctos, que son los minimizados">
<correction tzdata "Actualiza datos de Fiyi y de Palestina">
<correction udisks2 "Opciones de montaje: usa siempre errors=remount-ro para sistemas de ficheros ext [CVE-2021-3802]; usa la orden mkfs para formatear particiones exfat; añade «Recomienda exfatprogs» como la alternativa preferida">
<correction ulfius "Corrige el uso de funciones de asignación de memoria con ulfius_url_decode y ulfius_url_encode">
<correction vim "Corrige desbordamientos de memoria dinámica («heap») [CVE-2021-3770 CVE-2021-3778] y problema de «uso tras liberar» [CVE-2021-3796]; elimina alternativas a vim-gtk durante la transición vim-gtk -&gt; vim-gtk3, facilitando actualizaciones desde buster">
<correction wget "Corrige descargas de más de 2GB en sistemas de 32 bits">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2021 4980 qemu>
<dsa 2021 4981 firefox-esr>
<dsa 2021 4982 apache2>
<dsa 2021 4983 neutron>
<dsa 2021 4984 flatpak>
<dsa 2021 4985 wordpress>
<dsa 2021 4986 tomcat9>
<dsa 2021 4987 squashfs-tools>
<dsa 2021 4988 libreoffice>
<dsa 2021 4989 strongswan>
<dsa 2021 4992 php7.4>
<dsa 2021 4994 bind9>
<dsa 2021 4995 webkit2gtk>
<dsa 2021 4996 wpewebkit>
<dsa 2021 4998 ffmpeg>
<dsa 2021 5002 containerd>
<dsa 2021 5003 ldb>
<dsa 2021 5003 samba>
<dsa 2021 5004 libxstream-java>
<dsa 2021 5007 postgresql-13>
<dsa 2021 5008 node-tar>
<dsa 2021 5009 tomcat9>
<dsa 2021 5010 libxml-security-java>
<dsa 2021 5011 salt>
<dsa 2021 5013 roundcube>
<dsa 2021 5016 nss>
<dsa 2021 5017 xen>
<dsa 2021 5019 wireshark>
<dsa 2021 5020 apache-log4j2>
<dsa 2021 5022 apache-log4j2>
</table>



<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


