#use wml::debian::template title="Negociação de Conteúdo"
#use wml::debian::translation-check translation="c646e774c01abc2ee3d32c65d6920ea4f03159dc"

<H3>Como o servidor sabe qual arquivo servir</H3>
<P>Você irá perceber que os links internos não terminam em .html. Isso
ocorre porque o servidor está usando a negociação de conteúdo para
decidir qual versão do documento entregar. Quando houver mais de uma
escolha, o servidor fará uma lista de todos os arquivos que podem
ser servidos; por exemplo, se o pedido for por 'about', a lista
de possibilidades pode ser about.en.html e about.de.html.
O padrão para o servidor Debian é servir o documento em inglês,
mas pode ser alterado.

<P>Se um cliente tiver a variável correta configurada para servir
alemão, por exemplo, então o arquivo about.de.html será enviado.
Uma característica boa deste sistema é que se o idioma
desejado não estiver disponível, um idioma diferente será enviado em
seu lugar (esperamos que este seja melhor do que nada). A decisão de qual
documento será servido é um pouco confusa, assim ao invés de
descrevê-la aqui você pode ver a resposta definitiva em
<a href="https://httpd.apache.org/docs/current/content-negotiation.html">https://httpd.apache.org/docs/current/content-negotiation.html</a>
caso tenha interesse.

<P>Como muitos(as) usuários(as) não irão sequer saber da existência da
negociação de conteúdo, há links no final de cada página
apontando diretamente para a versão daquela página em cada
um dos outros idiomas disponíveis. Estes links são computados
por um script perl chamado pelo wml quando a página é gerada.

<P>Há também uma opção para substituir as preferências de idioma do
navegador usando um cookie que prioriza um único idioma sobre as
preferências do navegador.
