#use wml::debian::template title="Debian Med"
#use wml::debian::recent_list
# $Id$
#use wml::debian::translation-check translation="b9967d1f4d930716e9646a92bda776ced3c44cce"

<h2>Descrição do projeto</h2>

<p>O Debian Med é um
   <q><a href="https://cdd.alioth.debian.org/blends/">Debian Pure Blend</a></q>
   com o objetivo de desenvolver o Debian como um sistema operacional que
   seja particularmente adequado para as necessidades da prática médica e
   pesquisa biomédica.
   O objetivo do Debian Med é um sistema completamente livre e aberto para todas
   as tarefas na área de cuidados médicos e pesquisas. Para alcançar este objetivo,
   o Debian Med integra software livre e de código aberto para imagens médicas,
   bioinformática, infraestrutura de TI clínica, e outros no sistema
   operacional Debian.
</p>


<p>O Debian Med contém um conjunto de metapacotes que declaram
   dependências a outros pacotes Debian, e dessa forma o sistema completo
   é preparado para resolver determinadas tarefas.
   A melhor visão geral sobre o Debian Med é dada na
   <a href="https://debian-med.alioth.debian.org/tasks">página de tarefas</a>.
</p>

<p>Para uma discussão mais aprofundada, há
   <a href="https://people.debian.org/~tille/talks/">várias palestras sobre
   Debian Med e Debian Pure Blends em geral</a>
   disponíveis em slides, parte delas com gravações em vídeo.
</p>


<h2>Contato e informações para desenvolvedores(as)</h2>

<p>
A <a href="mailto:debian-med@lists.debian.org">lista de discussão
Debian Med</a> é o ponto central de comunicação para o Debian Med.
Ela serve como um fórum para potenciais, assim como atuais, usuários(as)
do sistema Debian que querem usar seus computadores para tarefas
médicas. Adicionalmente, é usada para coordenar esforços de
desenvolvimento sobre os diversos tópicos da Medicina. Você pode
se inscrever ou remover sua inscrição da lista usando a
<a href="https://lists.debian.org/debian-med/">página web da lista de
discussão</a>. Você também encontrará os arquivos da lista nessa página.
</p>
<p>
Locais importantes com informações para desenvolvedores(as):
</p>
<ol>
  <li><a href="https://wiki.debian.org/DebianMed">A página Wiki</a></li>
  <li><a href="https://blends.debian.org/med/">A página dos Blends</a></li>
  <li>As <a href="https://med-team.pages.debian.net/policy/">políticas do Debian
  Med</a> que explicam as regras de empacotamento na equipe</li>
  <li><a href="https://salsa.debian.org/med-team/">Repositórios Git dos pacotes
  do Debian Med no Salsa</a></li>
</ol>

<h2>Projetos de software incluídos</h2>

O sistema <a href="https://blends.debian.org/blends">Debian Pure Blends</a>
fornece uma visão geral concebida automaticamente de todos os softwares
incluídos em um Blend.
Basta observar nas chamadas
<b><a href="https://blends.debian.org/med/tasks/">páginas de tarefas do
Debian Med</a></b> para aprender sobre todos os softwares e projetos que estão
incluídos em nossa lista de tarefas para inclusão no Debian.


<h2>Objetivos do projeto</h2>

<ul>
  <li>Construir uma base sólida de softwares para a medicina com ênfase
      nas facilidades de instalação e manutenção e na segurança.</li>
  <li>Encorajar a cooperação de autores(as) de projetos de software diferentes
      com objetivos similares.</li>
  <li>Um conjunto de testes para facilitar a avaliação da qualidade de
      softwares médicos.</li>
  <li>Fornecer informações e documentação de softwares médicos.</li>
  <li>Ajudar os(as) autores(as) originais a conseguir empacotar seus produtos
      para o Debian.</li>
  <li>Demonstrar às empresas de software comerciais a força de um sistema
      com base sólida e fazê-los considerar a conversão de seus softwares
      para o Linux ou mesmo migrá-los para Código Aberto.</li>
</ul>


<h2>O que eu posso fazer para ajudar?</h2>

<p>
   Há uma <a href="https://wiki.debian.org/DebianMedTodo">página na wiki
   contendo uma lista de coisas</a> que podem ser feitas para ajudar o projeto.
</p>

<h2>Marketing &amp; RP</h2>

<p>Assim que tivermos alguma coisa para mostrar sobre este projeto, e certamente
   até nos estágios de formação do mesmo, estamos sendo observados pelo mundo
   todo. Necessariamente teremos que trabalhar com o press@debian.org para
   divulgar e ajudar o Debian e este projeto no tipo de exposição que desejamos.
   Para este propósito criaremos uma coleção de slides para palestras sobre o
   Debian Med.
</p>


<h2>Links</h2>

<ul>
   <li>O Debian Med está trabalhando em estreita colaboração com o
      <a href="http://nebc.nerc.ac.uk/tools/bio-linux/bio-linux-6.0">Bio-Linux</a>.
       Enquanto o Bio-Linux é baseado nas versões LTS do Ubuntu, os pacotes
       de escopo biológico são mantidos dentro do Debian pela equipe Debian Med.
  </li>
</ul>
