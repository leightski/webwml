msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2021-01-08 12:43+0100\n"
"Last-Translator: Szabolcs Siebenhofer\n"
"Language-Team: unknown\n"
"Language: Hungarian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/doc/books.data:35
msgid ""
"\n"
"  Debian 9 is the must-have handbook for learning Linux. Start on the\n"
"  beginners level and learn how to deploy the system with graphical\n"
"  interface and terminal.\n"
"  This book provides the basic knowledge to grow and become a 'junior'\n"
"  systems administrator. Start off with exploring the GNOME desktop\n"
"  interface and adjust it to your personal needs. Overcome your fear of\n"
"  using the Linux terminal and learn the most essential commands in\n"
"  administering Debian. Expand your knowledge of system services (systemd)\n"
"  and learn how to adapt them. Get more out of the software in Debian and\n"
"  outside of Debian. Manage your home-network with network-manager, etc.\n"
"  10 percent of the profits on this book will be donated to the Debian\n"
"  Project."
msgstr ""
"\n"
"   Debian 9 kötelező kézikönyv a Linux tanulásához. A kezdő szinten indul\n"
"   és megtanítja, hogyan üzemelj be egy rendszer grafikus interfésszel és\n"
"   terminállal.\n"
"   Ez a könyv az alap tudást biztosítja, hogy 'junior' rendszer-"
"adminisztrátorrá\n"
"   válj. Kezdve a Gnome asztali interfész felfedezésétől és a saját "
"igényeidhez\n"
"   való alakítástól. Legyőzi a félelmedet a Linux terminál használatától és\n"
"   megtanítja az alapevtő parancsokat a Debian adminisztrációjában. Növeli\n"
"   ismereteidet a rendszer szolgáltatásokban (systemd) és megtanít, hogyan "
"használd.\n"
"   Hozzál ki többet a Debian-ban és az azon kívül lévő szoftverekből. "
"Menedzseld\n"
"   otthoni hálózatodat a netwrok-manager-el, stb. A könyv értékesítésből "
"származó\n"
"   nyereség 10%-ával a Debian Projektet támogatjuk."

#: ../../english/doc/books.data:64 ../../english/doc/books.data:174
#: ../../english/doc/books.data:229
msgid ""
"Written by two Debian developers, this free book\n"
"  started as a translation of their French best-seller known as Cahier de\n"
"  l'admin Debian (published by Eyrolles). Accessible to all, this book\n"
"  teaches the essentials to anyone who wants to become an effective and\n"
"  independent Debian GNU/Linux administrator.\n"
"  It covers all the topics that a competent Linux administrator should\n"
"  master, from the installation and the update of the system, up to the\n"
"  creation of packages and the compilation of the kernel, but also\n"
"  monitoring, backup and migration, without forgetting advanced topics\n"
"  like SELinux setup to secure services, automated installations, or\n"
"  virtualization with Xen, KVM or LXC."
msgstr ""
"Két Debian fejlesztő írta, az a szabad könyv a francia best seller, a \n"
"   Cahier de l'admin Debian (az Eyeroll kiadásában) fordításaként "
"kezdődött.\n"
"   Mindenki számára elérhető, mindenkinek megtanítja az aalapokat, aki "
"szeretne\n"
"   hatékony és független Debian GNU/Linux adminisztrátor lenni.\n"
"   Lefed minden témakört, amit egy hozzáértő adminisztrátornak el kell "
"sajátítania\n"
"   a rendszer telepítésétől és frissítésétől kezdeve, a csomagok "
"létrehozásáig és\n"
"   a kernek fordításáig, de a felügyelet, mentés és a mingráció is része és "
"nincsenek\n"
"   elfelejtve olyan haladó témakörök, mint az SELinux beállítása "
"biztonságos\n"
"   szolgálatatásokhoz, automatizált telepítések vagy a vrirtualizáció Xen, "
"KVM vagy\n"
"   LXC használatával."

#: ../../english/doc/books.data:86
msgid ""
"The aim of this freely available book is to get you up to\n"
"  speed with Debian. It is comprehensive with basic support\n"
"  for the user who installs and maintains the system themselves (whether\n"
"  in the home, office, club, or school). Some Debian specific information "
"are very old.\n"
"  "
msgstr ""
"Ennek a szabadon elérhető könyvnek a segítségével felveheted a tempót\n"
"   a Debian-nal. Átfogóan nyújt alapvető segítséget a felhasználóknak\n"
"   akik maguknak telepítik és tartják karban saját rendszerüket (mindegy\n"
"   hogy otthon, az irodában, a klubban vagy az iskolában). Néhány Debian "
"specifikus\n"
"   információ nagyon régi."

#: ../../english/doc/books.data:108
msgid ""
"The first French book about Debian is already in its fifth edition. It\n"
"  covers all aspects of the administration of Debian from the installation\n"
"  to the configuration of network services.\n"
"  Written by two Debian developers, this book can be of interest to many\n"
"  people: the beginner wishing to discover Debian, the advanced user "
"looking\n"
"  for tips to enhance his mastership of the Debian tools and the\n"
"  administrator who wants to build a reliable network with Debian."
msgstr ""
"Az első francia könyv a Debian-ról, immár az ötödik kiadában. Lefedi a\n"
"   a Debian administrációjának valamennyi aspektusát a telepítéstől a "
"hálózati\n"
"   szolgáltatások beállításáig.\n"
"   Két Debian fejlesztő írta, az a könyv bárkit érdekelhet: a kezdő, aki\n"
"   szeretné felfedezni a Debian-t, a haladó felhasználó, aki szeretné "
"bővíteni\n"
"   tudását a Debian eszközeiben és az adminisztrátor, aki megbízható "
"hálózatot \n"
"   szeretne építeni Debian-nal."

#: ../../english/doc/books.data:128
msgid ""
"The book covers topics ranging from concepts of package\n"
"  management over the available tools and how they're used to concrete "
"problems\n"
"  which may occur in real life and how they can be solved. The book is "
"written\n"
"  in German, an English translation is planned. Format: e-book (Online, "
"HTML,\n"
"  PDF, ePub, Mobi), printed book planned.\n"
msgstr ""
"A könyv foglalkozik a csomagkezelés fogalmaitól kezdve az elérhető\n"
"   eszközökig és azok hogyan használhatóak konkrét eseteben, amik\n"
"   előfordulhatnak a való életben és hogyan oldhatóak meg. A könyv\n"
"   németül íródott, tervben van az angol fordítás. Formátum: e-book\n"
"   (Online, HTML, PDF, ePub, Mobi), a nyomtatott könyv tervben van.\n"

#: ../../english/doc/books.data:149
msgid ""
"This book teaches you how to install and configure the system and also how "
"to use Debian in a professional environment.  It shows the full potential of "
"the distribution (in its current version 8) and provides a practical manual "
"for all users who want to learn more about Debian and its range of services."
msgstr ""
"Ez a a könyv megtanít, hogyan telepítsd és konfiguráld a rendszert és "
"hogyanhasználd professzionális környezetben. Bemutatja a disztribúció teljes "
"potenciálját (a 8-as verzióban) és praktikus útmutatókat biztosít minden "
"felhasználó számára aki többet akar tanulni a Debian-ról és a szolgáltatásai "
"köréről."

#: ../../english/doc/books.data:203
msgid ""
"Written by two penetration researcher - Annihilator, Firstblood.\n"
"  This book teaches you how to build and configure Debian 8.x server system\n"
"  security hardening using Kali Linux and Debian simultaneously.\n"
"  DNS, FTP, SAMBA, DHCP, Apache2 webserver, etc.\n"
"  From the perspective that 'the origin of Kali Linux is Debian', it "
"explains\n"
"  how to enhance Debian's security by applying the method of penetration\n"
"  testing.\n"
"  This book covers various security issues like SSL cerificates, UFW "
"firewall,\n"
"  MySQL Vulnerability, commercial Symantec antivirus, including Snort\n"
"  intrusion detection system."
msgstr ""
"Két penetrációs kutató munkája - Annihilator és Firstblood.\n"
"   Ez a könyv megtanít téged, hogyan építés és konfigurálj Debian 8.x\n"
"   szerver rendszer biztonsági megerősítést, egyszerre használva Kali "
"Linuxot\n"
"   és Debian-t.\n"
"   DNS, FTP, SAMBA, DHCP, Apache2 webserver, stb.\n"
"   Abból a perspektívából, hogy a 'Kali Linux eredete a Debian', bemutatja\n"
"   hogyan javíthatod a Debian biztonságát a penetrációs tesztelés "
"metódusával.\n"
"   A könyv bemutat különböző biztonsági problémát, mint például a SSL\n"
"   tanusítványok, az UFW tűzfal, a MySQL sebezhetőség, a kereskedelmi "
"Symantech\n"
"   antivírus, és ide értve még a Snort behatolás detektáló rendszert."

#: ../../english/doc/books.def:38
msgid "Author:"
msgstr "Szerző:"

#: ../../english/doc/books.def:41
msgid "Debian Release:"
msgstr "Debián kiadás:"

#: ../../english/doc/books.def:44
msgid "email:"
msgstr "e-mail:"

#: ../../english/doc/books.def:48
msgid "Available at:"
msgstr "Elérhető:"

#: ../../english/doc/books.def:51
msgid "CD Included:"
msgstr "CD-t tartalmaz:"

#: ../../english/doc/books.def:54
msgid "Publisher:"
msgstr "Kiadó:"

#: ../../english/doc/manuals.defs:28
msgid "Authors:"
msgstr "Szerzők:"

#: ../../english/doc/manuals.defs:35
msgid "Editors:"
msgstr "Szerkesztők:"

#: ../../english/doc/manuals.defs:42
msgid "Maintainer:"
msgstr "Karbantartó:"

#: ../../english/doc/manuals.defs:49
msgid "Status:"
msgstr "Státusz:"

#: ../../english/doc/manuals.defs:56
msgid "Availability:"
msgstr "Elérhetőség:"

#: ../../english/doc/manuals.defs:85
msgid "Latest version:"
msgstr "Utolsó verzió:"

#: ../../english/doc/manuals.defs:101
msgid "(version <get-var version />)"
msgstr "(<get-var version />) verzió"

#: ../../english/doc/manuals.defs:131 ../../english/releases/arches.data:38
msgid "plain text"
msgstr "sima szöveg"

#: ../../english/doc/manuals.defs:147 ../../english/doc/manuals.defs:157
#: ../../english/doc/manuals.defs:165
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/git\">Git</a> repository."
msgstr ""

#: ../../english/doc/manuals.defs:149 ../../english/doc/manuals.defs:159
#: ../../english/doc/manuals.defs:167
msgid "Web interface: "
msgstr ""

#: ../../english/doc/manuals.defs:150 ../../english/doc/manuals.defs:160
#: ../../english/doc/manuals.defs:168
msgid "VCS interface: "
msgstr ""

#: ../../english/doc/manuals.defs:175 ../../english/doc/manuals.defs:179
msgid "Debian package"
msgstr "Debián csomag"

#: ../../english/doc/manuals.defs:184 ../../english/doc/manuals.defs:188
msgid "Debian package (archived)"
msgstr "Debian csomag (archivált)"

#: ../../english/releases/arches.data:36
msgid "HTML"
msgstr ""

#: ../../english/releases/arches.data:37
msgid "PDF"
msgstr ""

#~ msgid "Language:"
#~ msgstr "Nyelv:"

#~ msgid ""
#~ "Use <a href=\"cvs\">SVN</a> to download the SGML source text for <get-var "
#~ "ddp_pkg_loc />."
#~ msgstr ""
#~ "Használd <a href=\"cvs\">az SVN-t</a>, hogy letöltsd az SGML forrás "
#~ "szöveget az alábbihoz: <get-var ddp_pkg_loc />."

#~ msgid "CVS via web"
#~ msgstr "CVS a weben"

#~ msgid ""
#~ "CVS sources working copy: set <code>CVSROOT</code>\n"
#~ "  to <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
#~ "  and check out the <kbd>boot-floppies/documentation</kbd> module."
#~ msgstr ""
#~ "CVS forrás működő másolat: set <code>CVSROOT</code>\n"
#~ "  <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
#~ "  és check-out-old a <kbd>boot-floppies/documentation</kbd> modult."
