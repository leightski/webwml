#use wml::debian::translation-check translation="52b5638b41e422a23f94c13e491f7b2d778aa2b9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait deux problèmes dans viewvc, une interface
basée sur le web pour parcourir les dépôts Subversion et CVS. Les vecteurs
d’attaque impliquaient des fichiers avec des noms non sûrs qui, s’ils étaient
imbriqués dans un flux HTML, pouvaient faire que le navigateur exécute du code
non autorisé.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-22456">CVE-2023-22456</a>

<p>ViewVC, une interface de navigation pour les systèmes de gestion de versions
CVS et Subversion, avait une vulnérabilité de script intersite qui affectait
les versions avant 1.2.2 et 1.1.29. L’impact de cette vulnérabilité est atténué
par le besoin pour l’attaquant d’avoir les privilèges de commit sur un dépôt
Subversion exposé par une autre instance fiable de ViewVC. Le vecteur d’attaque
implique des fichiers avec des noms peu sûrs (noms qui s’ils étaient imbriqués
dans un flux HTML, pouvaient faire que le navigateur exécute du code non
autorisé), qui eux-mêmes pouvaient être difficiles à créer. Les utilisateurs doivent
mettre à jour au moins à la version 1.2.2 (s’ils utilisent une version 1.2.x
de ViewVC) ou 1.1.29 (s’ils utilisent une version 1.1.x). ViewVC 1.0.x n’est
plus pris en charge, aussi les utilisateurs de cette ancienne publication
devraient mettre en œuvre une solution de rechange. Les utilisateurs peuvent
éditer leurs patrons de vue EZT de ViewVC pour manuellement protéger contre
l’HTML les chemins modifiés lors du rendu. Repérez dans votre fichier
<q>revision.ezt</q> de l’ensemble de patrons les références de ces chemins
modifiés et enveloppez-les avec <q>[format "html"]</q> et <q>[end]</q>. Pour la
plupart des utilisateurs, cela signifie que les références à
<q>[changes.path]</q> deviennent <q>[format "html"][changes.path][end]</q>
(cette solution de rechange doit être annulée lors de la mise à niveau vers
une version corrigée de ViewVC, sinon les noms de chemins modifiés seront
doublement protégés).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-22464">CVE-2023-22464</a>

<p>ViewVC, une interface de navigation pour les systèmes de gestion de versions
CVS et Subversion, avait une vulnérabilité de script intersite qui affectait
les versions avant 1.2.3 et 1.1.30. L’impact de cette vulnérabilité est atténué
par le besoin pour l’attaquant d’avoir les privilèges de commit sur un dépôt
Subversion exposé par une autre instance fiable de ViewVC. Le vecteur d’attaque
implique des fichiers avec des noms peu sûrs (noms qui s’ils étaient imbriqués
dans un flux HTML, pouvaient faire que le navigateur exécute du code non
autorisé), qui eux-mêmes pouvaient être difficiles à créer. Les utilisateurs doivent
mettre à jour au moins à la version 1.2.3 (s’ils utilisent une version 1.2.x
de ViewVC) ou 1.1.30 (s’ils utilisent une version 1.1.x). ViewVC 1.0.x n’est
plus pris en charge, aussi les utilisateurs de cette ancienne publication
devraient mettre en œuvre une solution de rechange. Les utilisateurs peuvent
éditer leurs patrons de vue EZT de ViewVC pour manuellement protéger contre
l’HTML les chemins <q>copyfrom paths</q> lors du rendu. Repérez dans votre fichier
<q>revision.ezt</q> de l’ensemble de patrons les références de ces chemins
modifiés et enveloppez-les avec <q>[format "html"]</q> et <q>[end]</q>. Pour la
plupart des utilisateurs, cela signifie que les références à
<q>[changes.copy_path]</q> deviennent <q>[format "html"][changes.copy_path][end]</q>
(cette solution de rechange doit être annuler lors de la mise à niveau vers
une version corrigée de ViewVC, sinon les noms de chemins <q>copyfrom path</q>
seront doublement protégés).</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.1.26-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets viewvc.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3266.data"
# $Id: $
