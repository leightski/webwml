#use wml::debian::translation-check translation="a772cc0003a77241aec264fdbe1a71f0a49e4c74" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème a été découvert dans Nova, un projet d’OpenStack fournissant
un moyen de mise à disposition d’instances de calcul (c’est-à-dire des serveurs
virtuels). En fournissant une image simple VMDK spécialement créée qui
référençait un chemin spécifique de fichier de sauvegarde, un utilisateur
authentifié pouvait persuader des systèmes de renvoyer une copie du contenu de
fichier par le serveur, aboutissant à un accès non autorisé à des données
éventuellement sensibles.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2:18.1.0-6+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nova.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nova,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nova">\
https://security-tracker.debian.org/tracker/nova</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3302.data"
# $Id: $
