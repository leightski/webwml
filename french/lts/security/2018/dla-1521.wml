#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Fabien Arnoux a découvert plusieurs problèmes de sécurité dans la validation
de courriel du système otrs.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16586">CVE-2018-16586</a>

<p>Chargement d’image externe ou de ressources CSS dans le navigateur quand un
utilisateur ouvre un courriel malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16587">CVE-2018-16587</a>

<p>Effacement à distance de fichiers arbitraires par l’utilisateur du serveur
web OTRS ayant accès en écriture lors de l’ouverture d’un courriel malveillant.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.3.18-1+deb8u6.</p>
<p>Nous vous recommandons de mettre à jour vos paquets otrs2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1521.data"
# $Id: $
