#use wml::debian::translation-check translation="7473e749ff76a01d730fd1a14f3f28a88def2867" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans Python, un langage
interactif orienté objet de haut niveau.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14647">CVE-2018-14647</a>

<p>L’accélérateur C elementtree de Python échouait à initialiser le sel du
hachage d’Expat lors de l’initialisation. Cela pourrait faciliter la conduite
d’attaques de déni de service à l’encontre d’Expat en construisant un XML qui
causerait des collisions de hachage pathologiques dans des structures de données
internes de Expat, consommant des montants considérables de CPU et RAM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9636">CVE-2019-9636</a>

<p>Gestion impropre de l’encodage Unicode (avec un netloc incorrect) lors de la
normalisation NFKC aboutissant dans une divulgation d'informations (identifiants,
cookies, etc., mis en cache pour un nom d’hôte donné). Une URL contrefaite pour
l'occasion pourrait être incorrectement analysée pour localiser les cookies ou
les données d’authentification et envoyer ces informations à un hôte différent
que celui d’une analyse correcte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>

<p>Un problème a été découvert dans urllib2 où une injection CRLF
était possible si l’attaquant contrôlait un paramètre d’URL, comme prouvé par le
premier argument pour urllib.request.urlopen avec \r\n (spécialement dans la
chaîne de requête après un caractère ?) suivi par un en-tête HTTP ou un commande
Redis.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9947">CVE-2019-9947</a>

<p>Un problème a été découvert dans urllib2 où une injection CRLF
était possible si l’attaquant contrôlait un paramètre d’URL, comme prouvé par le
premier argument pour urllib.request.urlopen avec \r\n (spécialement dans la
chaîne de requête après un caractère ?) suivi par un en-tête HTTP ou un commande
Redis. Cela est similaire au problème de chaîne de requête
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.4.2-1+deb8u3.</p>
<p>Nous vous recommandons de mettre à jour vos paquets python3.4.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1835.data"
# $Id: $
