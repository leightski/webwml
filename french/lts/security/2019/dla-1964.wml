#use wml::debian::translation-check translation="b72bf093cf99ba165a739db567897930b7f7b462" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Dans sudo, un programme conçu pour donner des droits limités de
superutilisateur à des utilisateurs particuliers, un attaquant avec accès à un
compte sudoer Runas ALL peut contourner certaines listes noires de politiques et
de modules PAM de session, et peut provoquer une connexion incorrecte en
invoquant sudo avec un ID utilisateur contrefait. Par exemple, cela permet le
contournement de la configuration (ALL,!root) pour une commande « sudo -u#-1 ».</p>

<p>Consulter
<a href="https://www.sudo.ws/alerts/minus_1_uid.html">https://www.sudo.ws/alerts/minus_1_uid.html</a>
pour plus d’informations.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 1.8.10p3-1+deb8u6.</p>
<p>Nous vous recommandons de mettre à jour vos paquets sudo.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1964.data"
# $Id: $
