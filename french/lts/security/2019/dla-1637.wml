#use wml::debian::translation-check translation="3e2de9fda618337e90c6790731126da64318392f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Max Justicz a découvert une vulnérabilité dans APT, le gestionnaire de
paquet de haut niveau. Le code traitant les redirections HTTP dans la
méthode de transport HTTP ne vérifie pas correctement les champs transmis
sur le réseau. Cette vulnérabilité pourrait être utilisée par un attaquant
dans la position « d'homme du milieu » entre APT et un miroir pour injecter
un contenu malveillant dans la connexion HTTP. Ce contenu pourrait ensuite
être reconnu comme un paquet valable par APT et être utilisé plus tard pour
l'exécution de code avec les droits du superutilisateur sur la machine
cible.</p>

<p>Dans la mesure où la vulnérabilité est présente dans le gestionnaire de
paquets lui-même, il est recommandé de désactiver les redirections, afin de
prévenir son exploitation durant cette mise à niveau uniquement, avec les
instructions suivantes :</p>

<pre>
apt -o Acquire::http::AllowRedirect=false update
 apt -o Acquire::http::AllowRedirect=false upgrade
</pre>

<p>Cette désactivation est connue pour casser certains mandataires
lorsqu'elle est utilisée avec security.debian.org. Si cela arrive, les
personnes concernées peuvent modifier la source de sécurité d'APT afin
d'utiliser l'entrée :</p>

<pre>
 deb http://cdn-fastly.deb.debian.org/debian-security jessie/updates main
</pre>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la version 1.0.9.8.5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apt.</p>

<p>Instructions de mise à niveau particulières :</p>

<p>Si une mise à niveau avec APT sans redirection est impossible dans votre
situation, vous pouvez télécharger manuellement les fichiers (avec wget ou
curl) pour votre architecture avec les URL fournies ci-dessous, en
vérifiant que les hachages correspondent. Vous pouvez ensuite les installer
avec la commande dpkg -i.</p>

<p>Fichiers indépendants de l'architecture :</p>


<pre>
http://security.debian.org/debian-security/pool/updates/main/a/apt/apt-doc_1.0.9.8.5_all.deb
Size/SHA256 vérificationsum:301106 47df9567e45fadcd2a56c0fd3d514d8136f2f206aa7baa47405c6fcb94824ab6
http://security.debian.org/debian-security/pool/updates/main/a/apt/libapt-pkg-doc_1.0.9.8.5_all.deb
Size/SHA256 vérificationsum:750506 ce79b2ef272716b8da11f3fd0497ce0b7ee69c9c66d01669e8abbbfdde5e6256
</pre>

<p>Architecture amd64 :</p>


<pre>
http://security.debian.org/debian-security/pool/updates/main/a/apt/libapt-pkg4.12_1.0.9.8.5_amd64.deb
Size/SHA256 vérificationsum:792126 295d9c69854a4cfbcb46001b09b853f5a098a04c986fc5ae01a0124c1c27e6bd
http://security.debian.org/debian-security/pool/updates/main/a/apt/libapt-inst1.5_1.0.9.8.5_amd64.deb
Size/SHA256 vérificationsum:168896 f9615532b1577b3d1455fa51839ce91765f2860eb3a6810fb5e0de0c87253030
http://security.debian.org/debian-security/pool/updates/main/a/apt/apt_1.0.9.8.5_amd64.deb
Size/SHA256 vérificationsum:1109308 4078748632abc19836d045f80f9d6933326065ca1d47367909a0cf7f29e7dfe8
http://security.debian.org/debian-security/pool/updates/main/a/apt/libapt-pkg-dev_1.0.9.8.5_amd64.deb
Size/SHA256 vérificationsum:192950 09ef86d178977163b8cf0081d638d74e0a90c805dd77750c1d91354b6840b032
http://security.debian.org/debian-security/pool/updates/main/a/apt/apt-utils_1.0.9.8.5_amd64.deb
Size/SHA256 vérificationsum:368396 87c55d9ccadcabd59674873c221357c774020c116afd978fb9df6d2d0303abf2
http://security.debian.org/debian-security/pool/updates/main/a/apt/apt-transport-https_1.0.9.8.5_amd64.deb
Size/SHA256 vérificationsum:137230 f5a17422fd319ff5f6e3ea9a9e87d2508861830120125484130da8c1fd479df2
</pre>

<p>Architecture armel :</p>

<pre>
http://security.debian.org/debian-security/pool/updates/main/a/apt/libapt-pkg4.12_1.0.9.8.5_armel.deb
Size/SHA256 vérificationsum:717002 80fe021d87f2444abdd7c5491e7a4bf9ab9cb2b8e6fa72d308905f4e0aad60d4
http://security.debian.org/debian-security/pool/updates/main/a/apt/libapt-inst1.5_1.0.9.8.5_armel.deb
Size/SHA256 vérificationsum:166784 046fb962fa214c5d6acfb7344e7719f8c4898d87bf29ed3cd2115e3f6cdd14e9
http://security.debian.org/debian-security/pool/updates/main/a/apt/apt_1.0.9.8.5_armel.deb
Size/SHA256 vérificationsum:1067404 f9a257d6aace1f222633e0432abf1d6946bad9dbd0ca18dccb288d50f17b895f
http://security.debian.org/debian-security/pool/updates/main/a/apt/libapt-pkg-dev_1.0.9.8.5_armel.deb
Size/SHA256 vérificationsum:193768 4cb226f55132a68a2f5db925ada6147aaf052adb02301fb45fb0c2d1cfce36f0
http://security.debian.org/debian-security/pool/updates/main/a/apt/apt-utils_1.0.9.8.5_armel.deb
Size/SHA256 vérificationsum:353178 38042838d8bc79642e5389be7d2d2d967cbf316805d4c8c2d6afbe1bc164aacc
http://security.debian.org/debian-security/pool/updates/main/a/apt/apt-transport-https_1.0.9.8.5_armel.deb
Size/SHA256 vérificationsum:134932 755b6d22f5914f3153a1c15427e5221507b174c0a4c6b860ebd16234c9e9a146
</pre>

<p>Architecture armhf :</p>

<pre>
http://security.debian.org/debian-security/pool/updates/main/a/apt/libapt-pkg4.12_1.0.9.8.5_armhf.deb
Size/SHA256 vérificationsum:734302 0f48f6d0406afdf0bd4d39e90e56460fab3d9b5fa4c91e2dca78ec22caf2fe2a
http://security.debian.org/debian-security/pool/updates/main/a/apt/libapt-inst1.5_1.0.9.8.5_armhf.deb
Size/SHA256 vérificationsum:166556 284a1ffd529e1daab3c300be17a20f11450555be9c0af166d9796c18147a03ba
http://security.debian.org/debian-security/pool/updates/main/a/apt/apt_1.0.9.8.5_armhf.deb
Size/SHA256 vérificationsum:1078212 08d85c30c8e4a6df0dced8e232a6c7639caa231acef4af8fdee2c1e07f0178ba
http://security.debian.org/debian-security/pool/updates/main/a/apt/libapt-pkg-dev_1.0.9.8.5_armhf.deb
Size/SHA256 vérificationsum:193796 3a26bd79677b46ce0a992e2ac808c4bbd2d5b3fc37b57fc93c8efa114de1adaa
http://security.debian.org/debian-security/pool/updates/main/a/apt/apt-utils_1.0.9.8.5_armhf.deb
Size/SHA256 vérificationsum:357074 19dec9ffc0fe4a86d6e61b5213e75c55ae6aaade6f3804f90e2e4034bbdc44d8
http://security.debian.org/debian-security/pool/updates/main/a/apt/apt-transport-https_1.0.9.8.5_armhf.deb
Size/SHA256 vérificationsum:135072 06ba556c5218e58fd14119e3b08a08f685209a0cbe09f2328bd572cabc580bca
</pre>

<p>Architecture i386 :</p>

<pre>
http://security.debian.org/debian-security/pool/updates/main/a/apt/libapt-pkg4.12_1.0.9.8.5_i386.deb
Size/SHA256 vérificationsum:800840 201b6cf4625ed175e6a024ac1f7ca6c526ca79d859753c125b02cd69e26c349d
http://security.debian.org/debian-security/pool/updates/main/a/apt/libapt-inst1.5_1.0.9.8.5_i386.deb
Size/SHA256 vérificationsum:170484 5791661dd4ade72b61086fefdc209bd1f76ac7b7c812d6d4ba951b1a6232f0b9
http://security.debian.org/debian-security/pool/updates/main/a/apt/apt_1.0.9.8.5_i386.deb
Size/SHA256 vérificationsum:1110418 13c230e9c544b1e67a8da413046bf1728526372170533b1a23e70cc99c40a228
http://security.debian.org/debian-security/pool/updates/main/a/apt/libapt-pkg-dev_1.0.9.8.5_i386.deb
Size/SHA256 vérificationsum:193780 c5b1bfa913ea2e2e332c228f5c5fe4dbc11ab334d0551a68ba6e87e94a51ffee
http://security.debian.org/debian-security/pool/updates/main/a/apt/apt-utils_1.0.9.8.5_i386.deb
Size/SHA256 vérificationsum:371218 1a74b12c8bb6b3968a721f3aa96739073e4fe2ced9302792c533e21535bc9cf4
http://security.debian.org/debian-security/pool/updates/main/a/apt/apt-transport-https_1.0.9.8.5_i386.deb
Size/SHA256 vérificationsum:139036 32148d92914a97df8bbb9f223e788dcbc7c39e570cf48e6759cb483a65b68666
</pre>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1637.data"
# $Id: $
