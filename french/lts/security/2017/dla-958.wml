#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9224">CVE-2017-9224</a>

<p>Un problème a été découvert dans Oniguruma 6.2.0, tel qu’utilisé dans
Oniguruma-mod dans Ruby jusqu’à 2.4.1 et mbstring dans PHP jusqu’à 7.1.5. Une
lecture hors limites de pile se produit dans match_at() lors de recherche
par expression rationnelle. Une erreur de logique impliquant l’ordre de validation
et d’accès dans match_at() pourrait aboutir à une lecture hors limites d’un
tampon de pile.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9226">CVE-2017-9226</a>

<p>Un problème a été découvert dans Oniguruma 6.2.0, tel qu’utilisé dans
Oniguruma-mod dans Ruby jusqu’à 2.4.1 et mbstring dans PHP jusqu’à 7.1.5. Une
écriture ou lecture hors limites de tas se produit dans next_state_val() lors de
la compilation d’expression rationnelle. Des nombres octaux supérieurs à 0xff ne
sont pas gérés correctement dans fetch_token() et fetch_token_in_cc(). Une
expression rationnelle mal formée contenant un nombre octal de la forme « \700 »
produirait une valeur de point de code non valable et supérieure à 0xff dans
next_state_val(), aboutissant à une corruption de mémoire d'écriture hors
limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9227">CVE-2017-9227</a>

<p>Un problème a été découvert dans Oniguruma 6.2.0, tel qu’utilisé dans
Oniguruma-mod dans Ruby jusqu’à 2.4.1 et mbstring dans PHP jusqu’à 7.1.5. Une
lecture hors limites de pile se produit dans mbc_enc_len() lors de recherche
par expression rationnelle. Un traitement non valable de reg-&gt;dmin dans
forward_search_range() pourrait aboutir à un déréférencement de pointeur non
valable, tel qu’une lecture hors limites d’un tampon de pile.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9228">CVE-2017-9228</a>

<p>Un problème a été découvert dans Oniguruma 6.2.0, tel qu’utilisé dans
Oniguruma-mod dans Ruby jusqu’à 2.4.1 et mbstring dans PHP jusqu’à 7.1.5. Une
écriture hors limites de tas se produit dans bitset_set_range() lors de la
compilation d’expression rationnelle due à une variable non initialisées d’une
transition d’état incorrecte. Une telle transition dans parse_char_class()
pourrait créer un chemin d’exécution laissant une variable locale critique non
initialisée jusqu’à son exécution comme index, aboutissant à une corruption de
mémoire d’écriture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9229">CVE-2017-9229</a>

<p>Un problème a été découvert dans Oniguruma 6.2.0, tel qu’utilisé dans
Oniguruma-mod dans Ruby jusqu’à 2.4.1 et mbstring dans PHP jusqu’à 7.1.5. Un
SIGSEGV se produit dans left_adjust_char_head() lors de la compilation
d’expression rationnelle. Un traitement non valable de reg-&gt;dmax dans
forward_search_range() pourrait aboutir à un déréférencement de pointeur non
valable, généralement comme une condition de déni de service immédiat.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 5.9.1-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libonig.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-958.data"
# $Id: $
