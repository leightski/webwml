#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Grégoire Scano"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un bogue dans la gestion d'erreur du code d'envoi d'un fichier pour le connecteur NIO HTTP
avait pour résultat que l'objet Processeur courant soit ajouté au
cache du Processeur plusieurs fois. Par conséquence, le
Processeur pouvait être utilisé pour des requêtes concurrentes. Le partage d'un Processeur
peut amener à une fuite d'informations entre les requêtes incluant, de
façon non exhaustive, l'ID de session et le corps de la réponse.</p>

<p>De plus, cette mise à jour résout une régression lors de l'exécution de
Tomcat 7 avec le SecurityManager activé, et ce, en raison d'une correction incomplète pour
<a href="https://security-tracker.debian.org/tracker/CVE-2016-6816">CVE-2016-6816</a>.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 7.0.28-4+deb7u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-779.data"
# $Id: $
