#use wml::debian::translation-check translation="07965d116e2f997ea3420d09e8858d44344fa1bb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>dlt-daemon, un démon de journalisation (DLT – Diagnostic Log and Trace),
a eu les vulnérabilités suivantes signalées.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29394">CVE-2020-29394</a>

<p>Un dépassement de tampon dans la fonction dlt_filter_load dans dlt_common.c
de dlt-daemon permet l’exécution de code arbitraire à cause de la mauvaise
utilisation de fscanf (aucune limite dans le nombre de caractères à lire dans
l’argument de format).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36244">CVE-2020-36244</a>

<p>dlt-daemon était vulnérable à un dépassement de tampon de tas qui pouvait
permettre à un attaquant d’exécuter du code arbitraire à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31291">CVE-2022-31291</a>

<p>Un problème dans dlt_config_file_parser.c de dlt-daemon permet à des
attaquants de provoquer une double libération de zone de mémoire à l’aide de
paquets TCP contrefaits.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 2.18.0-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dlt-daemon.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de dlt-daemon,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/dlt-daemon">\
https://security-tracker.debian.org/tracker/dlt-daemon</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3231.data"
# $Id: $
