#use wml::debian::translation-check translation="7253c84c7ea15eedd4604227d821a828968e047a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour corrige plusieurs violations d’accès mémoire dans vim.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0318">CVE-2022-0318</a>

<p>Dépassement de tampon de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0392">CVE-2022-0392</a>

<p>Dépassement de tampon de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0629">CVE-2022-0629</a>

<p>Dépassement de pile.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0696">CVE-2022-0696</a>

<p>Déréférencement de pointeur <code>NULL</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1619">CVE-2022-1619</a>

<p>Dépassement de tampon de tas dans la fonction <code>cmdline_erase_chars</code>.
Ces vulnérabilités peuvent planter le logiciel, modifier la mémoire et permettre
une exécution à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1621">CVE-2022-1621</a>

<p>Dépassement de tampon de tas dans <code>vim_strncpy find_word</code>. Cette
vulnérabilité peut planter le logiciel, contourner les mécanismes de protection,
modifier la mémoire et permettre une exécution à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1785">CVE-2022-1785</a>

<p>Écriture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1897">CVE-2022-1897</a>

<p>Écriture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1942">CVE-2022-1942</a>

<p>Dépassement de tampon de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2000">CVE-2022-2000</a>

<p>Écriture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2129">CVE-2022-2129</a>

<p>Écriture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3235">CVE-2022-3235</a>

<p>Utilisation de mémoire après libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3256">CVE-2022-3256</a>

<p>Utilisation de mémoire après libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3352">CVE-2022-3352</a>

<p>Utilisation de mémoire après libération.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 2:8.1.0875-5+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets vim.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de vim,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/vim">\
https://security-tracker.debian.org/tracker/vim</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3204.data"
# $Id: $
