#use wml::debian::translation-check translation="e8ccef590b7be73555745f652383378a176517ab" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été découvertes dans gerbv, un visionneur de fichier
Gerber. La plupart des programmes de conception de circuit imprimé peuvent
exporter dans un fichier Gerber.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40401">CVE-2021-40401</a>:
Une vulnérabilité d’utilisation de mémoire après libération existait dans la
fonction d’analyse lexicale de définition de gravure (aperture) RS-274X. Un
fichier gerber contrefait spécialement pouvait conduire à une exécution de
code.</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40403">CVE-2021-40403</a>:
Un vulnérabilité de divulgation d'informations existait dans la fonctionnalité
d’analyse de rotation de système de placement (pick-and-place). Un fichier
contrefait spécialement pouvait exploiter l’absence d’initialisation d’une
structure pour une fuite du contenu de la mémoire.</li>
</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.7.0-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gerbv.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3210.data"
# $Id: $
