#use wml::debian::translation-check translation="801c6bd7145a4a5317233abb440f0a2d6d97e671" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait une vulnérabilité potentielle de lecture de fichier
arbitraire dans twig, une bibliothèque de modèles pour PHP. Elle était
provoquée par une validation insuffisante des noms de modèle dans les
instructions <q>source</q> et <q>include</q>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39261">CVE-2022-39261</a>

<p>Twig est un langage de modèle pour PHP. Les versions 1.x antérieures à
1.44.7, 2.x antérieures à 2.15.3 et 3.x antérieures à 3.4.3 étaient
confrontées à un problème quand le chargeur du système de fichiers charge
des modèles dont le nom est une entrée de l'utilisateur. Il est possible
d'utiliser les instructions <q>source</q> et <q>include</q> pour lire des
fichiers arbitraires en dehors du répertoire des modèles lors de
l'utilisation d'un espace de noms de type
<q>@quelque_part/../un.fichier</q>. Dans ce cas, la validation est
contournée. Les versions 1.44.7, 2.15.3 et 3.4.3 contiennent un correctif
pour la validation de ce type de nom de modèle. Il n'existe pas de
contournement connu à part la mise à niveau.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans la version
2.6.2-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets twig.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3147.data"
# $Id: $
