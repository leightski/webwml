#use wml::debian::translation-check translation="80bf7c3b8d930a30a3e818b33678009ed43ba653" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité d'utilisation de mémoire après libération a été
découverte dans Usbredirparser, un analyseur du protocole usbredir, qui
pouvait avoir pour conséquences un déni de service ou éventuellement
l'exécution de code arbitraire.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
0.7.1-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets usbredir.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de usbredir, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/usbredir">\
https://security-tracker.debian.org/tracker/usbredir</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2958.data"
# $Id: $
