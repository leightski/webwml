#use wml::debian::translation-check translation="3cbbe205c4de468660b7739a142c62a4a080ec06" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans MediaWiki, un 
moteur de site web pour un travail collaboratif.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41765">CVE-2022-41765</a>

<p>HTMLUserTextField exposait l'existence d'utilisateurs cachés.</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41767">CVE-2022-41767</a>

<p>Le script de maintenance reassignEdits ne mettait pas à jour les
résultats dans une vérification de plage d'IP dans Special:Contributions.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
1:1.31.16-1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mediawiki.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mediawiki, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/mediawiki">\
https://security-tracker.debian.org/tracker/mediawiki</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3148.data"
# $Id: $
