#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour inclut les modifications de tzdata jusqu'à la
version 2016h. Les modifications notables sont les suivantes :</p>

<ul>
<li>Gaza, Asie et Hébron Asie, fin de l'heure d'été le 29-10-2016 à 01:00,
et non le 21-10-2016 à 00:00 ;</li>
<li>Istanbul est passée de EET/EEST (+02/+03) à +03 de façon permanente le
07-09-2016. Dans la mesure où le fuseau horaire a changé, la divergence
avec EET/EEST aura lieu le 30-10-2016 ;</li>
<li>la Turquie est passée de EET/EEST (+02/+03) à +03 de façon permanente
le 7-09-2016 ;</li>
<li>nouvelle correction de la seconde intercalaire à 23:59:60 UTC le
31-12-2016 selon le Bulletin C 52 de l'IERS.</li>
</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2016h-0+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tzdata.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-681.data"
# $Id: $
