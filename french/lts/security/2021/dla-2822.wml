#use wml::debian::translation-check translation="414c21d930b5d563454655a029dc6f7499a7000b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans netkit-rsh, des programmes de
client et de serveur pour des connexions distantes d'interpréteur.
À cause d'une validation insuffisante des entrées dans les noms de chemin
envoyées par un serveur, un serveur malveillant peut réaliser l'écrasement
de fichiers arbitraires dans le répertoire cible ou modifier les droits du
répertoire cible.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 0.17-17+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets netkit-rsh.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de netkit-rsh, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/netkit-rsh">\
https://security-tracker.debian.org/tracker/netkit-rsh</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2822.data"
# $Id: $
