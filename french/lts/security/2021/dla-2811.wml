#use wml::debian::translation-check translation="318b124a1839dc38c78105049264e858951f3777" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités d'injection SQL ont été découvertes dans SQLAlchemy,
une boîte à outils SQL et de mapping objet-relationnel pour Python, quand
les paramètres ORDER BY ou GROUP BY peuvent être contrôlés par un
attaquant.</p>

<p>Attention : la fonction de contrainte de texte de SQLAlchemy est
rarement utilisée mais l'avertissement qui était émis auparavant est devenu
maintenant une ArgumentError ou, dans le cas des paramètres order_by() et
group_by(), une CompileError.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.0.15+ds1-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sqlalchemy.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sqlalchemy, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/sqlalchemy">\
https://security-tracker.debian.org/tracker/sqlalchemy</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2811.data"
# $Id: $
