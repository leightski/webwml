#use wml::debian::translation-check translation="b8d6778778e18740e9495014b51cb097495c5b51" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>PostgreSQL 9.6.23 corrige le problème de sécurité suivant :</p>

<p>Interdire davantage la renégociation SSL (Michael Paquier)</p>

<p>La renégociation a été désactivée depuis un certain temps, mais le serveur
pourrait continuer à coopérer avec une requête de renégociation initiée par
le client. Un renégociation contrefaite de manière malveillante pourrait
aboutir à un plantage du serveur (consultez le problème d’OpenSSL
<a href="https://security-tracker.debian.org/tracker/CVE-2021-3449">CVE-2021-3449</a>).
Désactivation complète de cette fonctionnalité dans toutes les versions
d’OpenSSL permettant de le faire, c’est-à-dire les versions 1.1.0h et
suivantes.</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 9.6.23-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets postgresql-9.6.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de postgresql-9.6,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/postgresql-9.6">\
https://security-tracker.debian.org/tracker/postgresql-9.6</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2751.data"
# $Id: $
