#use wml::debian::translation-check translation="9e8d55e7a67712bb45af959a8a45a69e1a9b167a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans mqtt-client où une déconversion de
paramètre de trame MQTT corrompue peut conduire à négocier une exception
d’épuisement de mémoire le rendant passif.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.14-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mqtt-client.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mqtt-client, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mqtt-client">\
https://security-tracker.debian.org/tracker/mqtt-client</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2582.data"
# $Id: $
