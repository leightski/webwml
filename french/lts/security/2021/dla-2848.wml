#use wml::debian::translation-check translation="82670096561006f6abaf230e8bdab3857b9e6704" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes ont été découverts dans libssh2, une bibliothèque C coté
client, mettant en œuvre le protocole SSH2 :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13115">CVE-2019-13115</a> :

<p>kex_method_diffie_hellman_group_exchange_sha256_key_exchange dans kex.c
a un dépassement d'entier qui pourrait conduire à une lecture hors limites
dans la manière dont les paquets sont lus à partir du serveur. Un attaquant
distant qui compromet un serveur SSH peut être capable de divulguer des
informations sensibles ou de provoquer des conditions de déni de service sur
le système client quand un utilisateur se connecte au serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17498">CVE-2019-17498</a> :

<p>La logique SSH_MSG_DISCONNECT dans packet.c a un dépassement d'entier
dans une vérification de limites permettant à un attaquant de spécifier un
décalage arbitraire (hors limites) pour une lecture de mémoire ultérieure.
Un serveur SSH contrefait peut être capable de divulguer des informations
sensibles ou de provoquer des conditions de déni de service sur le système
client quand un utilisateur se connecte au serveur.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.7.0-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libssh2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libssh2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libssh2">\
https://security-tracker.debian.org/tracker/libssh2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2848.data"
# $Id: $
