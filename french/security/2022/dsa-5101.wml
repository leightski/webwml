#use wml::debian::translation-check translation="e0c226c0aed6aed5fcbf971617b61c5255b0f1f4" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Emmet Leahy a signalé que libphp-adodb, une bibliothèque PHP de couche
d'abstraction de base de données, permettait d'injecter des valeurs dans la
chaîne de connexion de PostgreSQL. Selon la manière dont la bibliothèque
est utilisée, ce défaut peut avoir pour conséquences un contournement
d'authentification, la divulgation de l'adresse IP d'un serveur ou tout
autre impact non précisé.</p>

<p>Pour la distribution oldstable (Buster), ce problème a été corrigé dans
la version 5.20.14-1+deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 5.20.19-1+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libphp-adodb.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libphp-adodb,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libphp-adodb">\
https://security-tracker.debian.org/tracker/libphp-adodb</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5101.data"
# $Id: $
