#use wml::debian::translation-check translation="e61f99c4627e187c60d6bbaa4dc6bb5e6feb2e50" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Jeffrey Bencteux a signalé deux vulnérabilités dans cifs-utils, les
utilitaires du système de fichier CIFS (Common Internet File System) qui
peuvent avoir pour conséquences une élévation de privilèges
(<a href="https://security-tracker.debian.org/tracker/CVE-2022-27239">CVE-2022-27239</a>)
ou une fuite d'informations
(<a href="https://security-tracker.debian.org/tracker/CVE-2022-29869">CVE-2022-29869</a>).</p>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 2:6.8-2+deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2:6.11-3.1+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cifs-utils.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cifs-utils, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cifs-utils">\
https://security-tracker.debian.org/tracker/cifs-utils</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5157.data"
# $Id: $
