#use wml::debian::translation-check translation="282fe47153ee7ae459dbd068bec0e572c214acb8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans wpa_supplicant et
hostapd.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12695">CVE-2020-12695</a>

<p>hostapd ne gère pas correctement les messages de souscription UPnP sous
certaines conditions, permettant à un attaquant de provoquer un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-0326">CVE-2021-0326</a>

<p>wpa_supplicant ne traite pas correctement les informations du groupe P2P
(Wi-Fi Direct) reçues des propriétaires actifs du groupe. Un attaquant à
portée d'émetteur du périphérique exécutant P2P pourrait tirer avantage de
ce défaut pour provoquer un déni de service ou éventuellement exécuter du
code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27803">CVE-2021-27803</a>

<p>wpa_supplicant ne traite pas correctement les requêtes P2P (Wi-Fi
Direct) de recherche de disponibilité. Un attaquant à portée d'émetteur du
périphérique exécutant P2P pourrait tirer avantage de ce défaut pour
provoquer un déni de service ou éventuellement exécuter du code arbitraire.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2:2.7+git20190128+0c1e29f-6+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wpa.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wpa, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4898.data"
# $Id: $
