#use wml::debian::translation-check translation="4924e09e5cb1b4163d7ec7161488354e4167b24c"
<define-tag pagetitle>Debian 10 is bijgewerkt: 10.13 werd uitgebracht</define-tag>
<define-tag release_date>2022-09-10</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.13</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de dertiende (en laatste) update aan van zijn
vorige stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Na deze tussenrelease zullen de beveiligings- en releaseteams van Debian geen updates meer uitbrengen voor Debian 10. Gebruikers die beveiligingsondersteuning willen blijven ontvangen, moeten opwaarderen naar Debian 11 of <url "https://wiki.debian.org/LTS"> raadplegen voor details over de deelverzameling van architecturen en pakketten die door het project voor langetermijnondersteuning wordt gedekt.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van oldstable, de vroegere stabiele release, worden een paar
belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction adminer "Oplossen van een open omleidingsprobleem en problemen met cross-site scripting [CVE-2020-35572 CVE-2021-29625]; elasticsearch: reactie niet afdrukken als HTTP-code niet 200 is [CVE-2021-21311]; een gecompileerde versie en configuratiebestanden leveren">
<correction apache2 "Oplossen van een probleem van denial of service [CVE-2022-22719], een probleem van smokkelen van HTTP-verzoeken [CVE-2022-22720], een probleem met overlopen van gehele getallen [CVE-2022-22721], een probleem van schrijven buiten de grenzen [CVE-2022-23943], een probleem van smokkelen van HTTP-verzoeken [CVE-2022-26377], problemen met lezen buiten de grenzen [CVE-2022-28614 CVE-2022-28615], een probleem van denial of service [CVE-2022-29404], een probleem van lezen buiten de grenzen [CVE-2022-30556], mogelijk probleem met het omzeilen van authenticatie op basis van IP [CVE-2022-31813]">
<correction base-files "Update voor tussenrelease 10.13">
<correction clamav "Nieuwe bovenstroomse stabiele release; beveiligingsoplossingen [CVE-2022-20770 CVE-2022-20771 CVE-2022-20785 CVE-2022-20792 CVE-2022-20796]">
<correction commons-daemon "JVM-detectie herstellen">
<correction composer "Kwetsbaarheid voor code-injectie oplossen [CVE-2022-24828]; het tokenpatroon voor GitHub bijwerken; de kopregel Authorization gebruiken in plaats van de verouderde zoekopdrachtparameter access_token">
<correction debian-installer "Hercompilatie tegen buster-proposed-updates; Linux ABI verhogen naar 4.19.0-21">
<correction debian-installer-netboot-images "Hercompilatie tegen buster-proposed-updates; Linux ABI verhogen naar 4.19.0-21">
<correction debian-security-support "Bijwerken van de beveiligingstoestand van verschillende pakketten">
<correction debootstrap "Ervoor zorgen dat chroots met niet-samengevoegde usr-mappen aangemaakt kunnen blijven worden voor oudere releases en buildd-chroots">
<correction distro-info-data "Toevoegen van Ubuntu 22.04 LTS, Jammy Jellyfish en Ubuntu 22.10, Kinetic Kudu">
<correction dropbear "Oplossen van mogelijk probleem van gebruikersnaamopsomming [CVE-2019-12953]">
<correction eboard "Oplossing voor segmentatiefout bij de selectie van de krachtbron">
<correction esorex "Oplossing voor mislukken van testsuite op armhf en ppc64el door verkeerd gebruik van libffi">
<correction evemu "Oplossen van compilatiefout met recente kernelversies">
<correction feature-check "Sommige versievergelijkingen oplossen">
<correction flac "Oplossen van probleem van schrijven buiten de grenzen [CVE-2021-0561]">
<correction foxtrotgps "Oplossing voor het mislukken van de compilatie met recentere versies van imagemagick">
<correction freeradius "Oplossing voor zijkanaallek waarbij 1 op 2048 handdrukken mislukken [CVE-2019-13456], probleem van denial of service door een multi-threaded BN_CTX-toegang [CVE-2019-17185], crash vanwege niet-thread-veilige geheugentoewijzing">
<correction freetype "Oplossing voor probleem van bufferoverloop [CVE-2022-27404]; oplossing voor crashes [CVE-2022-27405 CVE-2022-27406]">
<correction fribidi "Oplossing voor problemen van bufferoverloop [CVE-2022-25308 CVE-2022-25309]; oplossing voor crash [CVE-2022-25310]">
<correction ftgl "Niet proberen om voor latex PNG naar EPS te converteren, omdat in onze imagemagick EPS om veiligheidsredenen uitgeschakeld werd">
<correction gif2apng "Oplossen van problemen met stapelbufferoverloop [CVE-2021-45909 CVE-2021-45910 CVE-2021-45911]">
<correction gnucash "Oplossing voor mislukkende compilatie met recente tzdata">
<correction gnutls28 "Reparatie voor testsuite bij combinatie met OpenSSL 1.1.1e of nieuwer">
<correction golang-github-docker-go-connections "Tests met verlopen certificaten overslaan">
<correction golang-github-pkg-term "Reparatie voor compilatie op recentere 4.19 kernels">
<correction golang-github-russellhaering-goxmldsig "Oplossen van probleem van NULL pointer dereference [CVE-2020-7711]">
<correction grub-efi-amd64-signed "Nieuwe bovenstroomse release">
<correction grub-efi-arm64-signed "Nieuwe bovenstroomse release">
<correction grub-efi-ia32-signed "Nieuwe bovenstroomse release">
<correction grub2 "Nieuwe bovenstroomse release">
<correction htmldoc "Oplossen van oneindige lus [CVE-2022-24191], problemen met overloop van gehele getallen [CVE-2022-27114] en probleem met stapelbufferoverloop [CVE-2022-28085]">
<correction iptables-netflow "Regressie met DKMS-compilatiefouten repareren veroorzaakt door bovenstroomse wijzigingen in de 4.19.191 Linux-kernel">
<correction isync "Oplossing voor problemen met bufferoverloop [CVE-2021-3657]">
<correction kannel "Bouwfout opgelost door uitschakeling van genereren van Postscript-documentatie">
<correction krb5 "SHA256 gebruiken als frommel voor Pkinit CMS">
<correction libapache2-mod-auth-openidc "Verbetering van de validatie van de post-logout URL-parameter bij het afmelden [CVE-2019-14857]">
<correction libdatetime-timezone-perl "Bijwerken van opgenomen gegevens">
<correction libhttp-cookiejar-perl "Fout bij het bouwen oplossen door de vervaldatum van een testcookie te verhogen">
<correction libnet-freedb-perl "De standaard computernaam veranderen van het ter ziele gegane freedb.freedb.org in gnudb.gnudb.org">
<correction libnet-ssleay-perl "Reparatie voor testfouten met OpenSSL 1.1.1n">
<correction librose-db-object-perl "Reparatie voor testfout na 6/6/2020">
<correction libvirt-php "Reparatie voor segmentatiefout in libvirt_node_get_cpu_stats">
<correction llvm-toolchain-13 "Nieuw broncodepakket om het bouwen van recentere versies van firefox-esr en thunderbird te ondersteunen">
<correction minidlna "HTTP-verzoeken valideren om te beschermen tegen DNS rebinding-aanvallen [CVE-2022-26505]">
<correction mokutil "Nieuwe bovenstroomse versie om SBAT-beheer mogelijk te maken">
<correction mutt "Probleem met uudecode-bufferoverloop oplossen [CVE-2022-1328]">
<correction node-ejs "Opties en nieuwe objecten uitzuiveren [CVE-2022-29078]">
<correction node-end-of-stream "Omzeilen van testbug">
<correction node-minimist "Probleem met prototypepollutie oplossen [CVE-2021-44906]">
<correction node-node-forge "Problemen met handtekeningverificatie oplossen [CVE-2022-24771 CVE-2022-24772 CVE-2022-24773]">
<correction node-require-from-string "Reparatie voor een test in combinatie met nodejs &gt;= 10.16">
<correction nvidia-graphics-drivers "Nieuwe bovenstroomse release">
<correction nvidia-graphics-drivers-legacy-390xx "Nieuwe bovenstroomse release; oplossen van problemen van schrijven buiten de grenzen [CVE-2022-28181 CVE-2022-28185]; beveiligingsoplossingen [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction octavia "Controles van clientcertificaten repareren [CVE-2019-17134]; correct detecteren dat de agent op Debian actief is; sjabloon repareren dat vrrp-controlescript genereert; extra runtime-vereisten toevoegen; extra configuratie rechtstreeks meeleveren in het pakket van de agent">
<correction orca "Reparatie voor het gebruik met WebKitGTK 2.36">
<correction pacemaker "Relatieversies bijwerken om upgrades van stretch LTS op te lossen">
<correction pglogical "Oplossen van compilatiefout">
<correction php-guzzlehttp-psr7 "Onjuiste ontleding van kopregels repareren [CVE-2022-24775]">
<correction postfix "Nieuwe bovenstroomse stabiele release; door gebruiker ingesteld default_transport niet opheffen; if-up.d: niet afsluiten met een foutmelding als postfix nog geen mail kan versturen; reparatie voor dubbele vermeldingen van bounce_notice_recipient in postconf-uitvoer">
<correction postgresql-common "pg_virtualenv: tijdelijk wachtwoordbestand neerschrijven voor de eigenaar van het bestand aangepast wordt">
<correction postsrsd "Mogelijk probleem van denial of service oplossen wanneer Postfix bepaalde lange gegevensvelden verzendt, zoals meerdere samengevoegde e-mailadressen [CVE-2021-35525]">
<correction procmail "Oplossing voor NULL pointer dereference">
<correction publicsuffix "Bijwerken van opgenomen gegevens">
<correction python-keystoneauth1 "Bijwerken van tests om compilatiefouten op te lossen">
<correction python-scrapy "Geen authenticatiegegevens meesturen met alle verzoeken [CVE-2021-41125]; bij het doorsturen geen cookies blootgeven over de domeinen heen [CVE-2022-0577]">
<correction python-udatetime "Correct linken met de libm-bibliotheek">
<correction qtbase-opensource-src "Repareren van setTabOrder voor samengestelde elementen; een uitbreidingslimiet toevoegen voor XML-entiteiten [CVE-2015-9541]">
<correction ruby-activeldap "Toevoegen van ontbrekende vereiste ruby-builder">
<correction ruby-hiredis "Enkele onbetrouwbare tests overslaan om een compilatiefout te verhelpen">
<correction ruby-http-parser.rb "Oplossen van compilatiefout bij het gebruik van http-parser met de oplossing voor CVE-2019-15605">
<correction ruby-riddle "Het gebruik van <q>LOAD DATA LOCAL INFILE</q> toestaan">
<correction sctk "<q>pdftoppm</q> gebruiken in plaats van <q>convert</q> voor de conversie van PDF naar JPEG omdat dit laatste mislukt na het gewijzigde veiligheidsbeleid van ImageMagick">
<correction twisted "Oplossingen voor probleem met onjuiste URI en HTTP methodevalidatie [CVE-2019-12387], met onjuiste certificaatvalidatie in ondersteuning voor XMPP [CVE-2019-12855], problemen van HTTP/2 denial-of-service, problemen van smokkelen van HTTP-verzoeken [CVE-2020-10108 CVE-2020-10109 CVE-2022-24801], probleem met openbaarmaking van informatie bij het volgen van domeinoverschrijdende omleidingen [CVE-2022-21712], probleem van denial of service tijdens SSH-handdruk [CVE-2022-21716]">
<correction tzdata "Tijdzonegegevens voor Iran, Chili en Palestina bijwerken; lijst met schrikkelseconden bijwerken">
<correction ublock-origin "Nieuwe bovenstroomse stabiele release">
<correction unrar-nonfree "Oplossen van probleem van mapoverschrijding [CVE-2022-30333]">
<correction wireshark "Oplossen van een probleem van uitvoeren van code van buitenaf [CVE-2021-22191], van problemen van denial of service [CVE-2021-4181 CVE-2021-4184 CVE-2021-4185 CVE-2022-0581 CVE-2022-0582 CVE-2022-0583 CVE-2022-0585 CVE-2022-0586]">
</table>


<h2>Beveiligingsupdates</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de
vroegere stabiele release. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2021 4836 openvswitch>
<dsa 2021 4852 openvswitch>
<dsa 2021 4906 chromium>
<dsa 2021 4911 chromium>
<dsa 2021 4917 chromium>
<dsa 2021 4981 firefox-esr>
<dsa 2022 5034 thunderbird>
<dsa 2022 5044 firefox-esr>
<dsa 2022 5045 thunderbird>
<dsa 2022 5069 firefox-esr>
<dsa 2022 5074 thunderbird>
<dsa 2022 5077 librecad>
<dsa 2022 5080 snapd>
<dsa 2022 5086 thunderbird>
<dsa 2022 5090 firefox-esr>
<dsa 2022 5094 thunderbird>
<dsa 2022 5097 firefox-esr>
<dsa 2022 5106 thunderbird>
<dsa 2022 5108 tiff>
<dsa 2022 5109 faad2>
<dsa 2022 5111 zlib>
<dsa 2022 5113 firefox-esr>
<dsa 2022 5115 webkit2gtk>
<dsa 2022 5118 thunderbird>
<dsa 2022 5119 subversion>
<dsa 2022 5122 gzip>
<dsa 2022 5123 xz-utils>
<dsa 2022 5126 ffmpeg>
<dsa 2022 5129 firefox-esr>
<dsa 2022 5131 openjdk-11>
<dsa 2022 5132 ecdsautils>
<dsa 2022 5135 postgresql-11>
<dsa 2022 5137 needrestart>
<dsa 2022 5138 waitress>
<dsa 2022 5139 openssl>
<dsa 2022 5140 openldap>
<dsa 2022 5141 thunderbird>
<dsa 2022 5142 libxml2>
<dsa 2022 5143 firefox-esr>
<dsa 2022 5144 condor>
<dsa 2022 5145 lrzip>
<dsa 2022 5147 dpkg>
<dsa 2022 5149 cups>
<dsa 2022 5150 rsyslog>
<dsa 2022 5151 smarty3>
<dsa 2022 5152 spip>
<dsa 2022 5153 trafficserver>
<dsa 2022 5154 webkit2gtk>
<dsa 2022 5156 firefox-esr>
<dsa 2022 5157 cifs-utils>
<dsa 2022 5158 thunderbird>
<dsa 2022 5159 python-bottle>
<dsa 2022 5160 ntfs-3g>
<dsa 2022 5164 exo>
<dsa 2022 5165 vlc>
<dsa 2022 5167 firejail>
<dsa 2022 5169 openssl>
<dsa 2022 5171 squid>
<dsa 2022 5172 firefox-esr>
<dsa 2022 5173 linux-latest>
<dsa 2022 5173 linux-signed-amd64>
<dsa 2022 5173 linux-signed-arm64>
<dsa 2022 5173 linux-signed-i386>
<dsa 2022 5173 linux>
<dsa 2022 5174 gnupg2>
<dsa 2022 5175 thunderbird>
<dsa 2022 5176 blender>
<dsa 2022 5178 intel-microcode>
<dsa 2022 5181 request-tracker4>
<dsa 2022 5182 webkit2gtk>
<dsa 2022 5185 mat2>
<dsa 2022 5186 djangorestframework>
<dsa 2022 5188 openjdk-11>
<dsa 2022 5189 gsasl>
<dsa 2022 5190 spip>
<dsa 2022 5193 firefox-esr>
<dsa 2022 5194 booth>
<dsa 2022 5195 thunderbird>
<dsa 2022 5196 libpgjava>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten werden verwijderd wegens omstandigheden waarover wij geen controle hebben:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction elog "Onbeheerd; veiligheidsproblemen">
<correction libnet-amazon-perl "Vereist een verwijderd API">

</table>

<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in oldstable, de vroegere stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige distributie oldstable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Voorgestelde updates voor de distributie oldstable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>informatie over de distributie oldstable (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een vereniging van ontwikkelaars van vrije software
die vrijwillig tijd en moeite steken in het produceren van het volledig vrije
besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>


