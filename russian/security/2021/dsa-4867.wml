#use wml::debian::translation-check translation="938f7bfa52fa35f75b35de70bbd10bfc81e42e8d" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В загрузчике GRUB2 было обнаружено несколько уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14372">CVE-2020-14372</a>

    <p>Было обнаружено, что команда acpi позволяет привилегированному пользователю
    загружать специально сформированные ACPI-таблицы при включённом Secure Boot.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25632">CVE-2020-25632</a>

    <p>В команде rmmod было обнаружено использование указателей после освобождения памяти.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25647">CVE-2020-25647</a>

    <p>В функции grub_usb_device_initialize() была обнаружена запись за пределами
    выделенного буфера памяти. Функция вызывается для выполнения инициализации
    USB-устройств.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27749">CVE-2020-27749</a>

    <p>В grub_parser_split_cmdline было обнаружено переполнение буфера.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27779">CVE-2020-27779</a>

    <p>Было обнаружено, что команда cutmem позволяет привилегированному пользователю
    удалять области памяти при включённом Secure Boot.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20225">CVE-2021-20225</a>

    <p>В коде для грамматического разбора опций краткого вида была обнаружена
    запись за пределами выделенного буфера динамической памяти.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20233">CVE-2021-20233</a>

    <p>Была обнаружена запись за пределами выделенного буфера динамической памяти, вызываемое
    некорректным подсчётом требуемого места для закавычивания в коде отрисовки меню.</p></li>

</ul>

<p>Дополнительную информацию можно найти по адресу
<a href="https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot">\
https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot</a></p>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 2.02+dfsg1-20+deb10u4.</p>

<p>Рекомендуется обновить пакеты grub2.</p>

<p>С подробным статусом поддержки безопасности grub2 можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/grub2">\
https://security-tracker.debian.org/tracker/grub2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4867.data"
