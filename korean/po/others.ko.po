# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Song Woo-il, 2006
# Sebul, 2017, 2022
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-05-04 16:17+0900\n"
"Last-Translator: Sebul <sebuls@gmail.com>\n"
"Language-Team: debian-kr <debian-l10n-korean@lists.debian.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "새 멤버 코너"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "1단계"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "2단계"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "3단계"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "4단계"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "5단계"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "6단계"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "7단계"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "신청자 체크리스트"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "더 많은 정보"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "전화"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "팩스"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "주소"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "제품"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "티셔츠"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "모자"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "스티커"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "머그"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "다른 옷"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "폴로 셔츠"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "프리스비 원반"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "마우스 패드"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "배지"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr ""

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "귀고리"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "여행가방"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "우산"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr ""

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr ""

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "스위스 군용 칼"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "USB 스틱"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "랜야드"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "기타"

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr "가능한 언어:"

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr "국제 배송:"

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr ""

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr ""

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr ""

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr ""

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr ""

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr ""

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr ""

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr ""

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr ""

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr ""

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "위와 같음"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "데비안을 얼마나 오래 썼나요?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "데비안 개발자인가요?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr ""

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "당신에 대해 좀 더..."
